var cameras = [];

var Camera = function(options){
	this.x = check(options,'x',0);
	this.y = check(options,'y',0);
	this.z = check(options,'z',1);
	this.centreX = check(options,'centreX',window.innerWidth/2);
	this.centreY = check(options,'centreY',window.innerHeight/2);
	this.left = false;
	this.up = false;
	this.right = false;
	this.down = false;
	this.in = false;
	this.out = false;
	this.vitesse = 8;
	cameras.push(this);
}

Camera.prototype.move = function(){
	if(this.up==true){
		this.y=this.y-(1*this.vitesse)/this.z;
	}
	if(this.right==true){
		this.x=this.x+(1*this.vitesse)/this.z;
	}
	if(this.left==true){
		this.x=this.x-(1*this.vitesse)/this.z;
	}
	if(this.down==true){
		this.y=this.y+(1*this.vitesse)/this.z;
	}
	if(this.in==true){
		this.z=this.z*(1+this.vitesse/350);
	}
	if(this.out==true){
		this.z=this.z/(1+this.vitesse/350);
	}
}

var elements={
	all:[],
	interface:[]
};

var Element = function(options){
	this.name = check(options,'name','el'+elements.length);
	this.camera = check(options,'camera',cameras[0]);
	this.x = this.camera.centreX-(this.camera.centreX-check(options,'x',0)+this.camera.x*this.camera.z)/this.camera.z;
	this.y = this.camera.centreY-(this.camera.centreY-check(options,'y',0)+this.camera.y*this.camera.z)/this.camera.z;
	this.w = check(options,'w',0)/this.camera.z;
	this.h = check(options,'h',0)/this.camera.z;
	this.z = check(options,'z',1);
	this.rect = check(options,'rect',{});
	this.parent = check(options,'parent',document.body);
	this.dom = document.createElement(check(options,'dom','div'));
	this.dom.style.zIndex=elements.length;
	this.dom.id=this.name;
	elements.push(this);
}

Element.prototype.update = function(){
	if(document.getElementById(this.name)==null){
		this.parent.appendChild(this.dom);
	}

	this.rect={
		view:20*(this.z-this.camera.z)/this.camera.z,
		x:this.camera.centreX-(this.camera.centreX-this.x-this.camera.x)*this.camera.z,
		y:this.camera.centreY-(this.camera.centreY-this.y-this.camera.y)*this.camera.z,
		w:this.w*this.camera.z,
		h:this.h*this.camera.z,
		opacity:(this.w*this.camera.z-this.w*this.z)/2500
	}

	if(this.rect.x + this.rect.w > this.rect.view && this.rect.y + this.rect.h > this.rect.view && this.rect.x < window.innerWidth-this.rect.view && this.rect.y < window.innerHeight-this.rect.view && this.rect.opacity<1){
		updateCSS(this.dom,{
			marginLeft:this.rect.x+'px',
			marginTop:this.rect.y+'px',
			width:this.rect.w+'px',
			height:this.rect.h+'px',
			opacity:1-this.rect.opacity,
			fontSize:1/(this.z/this.camera.z)+'em',
		});
	}else{
		this.parent.removeChild(this.dom);
	}
}

document.onmouseup = function(e){
	//var element = new Element({x:e.clientX,y:e.clientY,z:cameras[0].z});
}

document.onkeydown = function(e){
	console.log(e.keyCode);
	if(e.keyCode==37){
		cameras[0].left=true;
	}
	if(e.keyCode==38){
		cameras[0].up=true;
	}
	if(e.keyCode==39){
		cameras[0].right=true;
	}
	if(e.keyCode==40){
		cameras[0].down=true;
	}
	if(e.keyCode==107){
		cameras[0].in=true;
	}
	if(e.keyCode==109){
		cameras[0].out=true;
	}
}

document.onkeyup = function(e){
	if(e.keyCode==37){
		cameras[0].left=false;
	}
	if(e.keyCode==38){
		cameras[0].up=false;
	}
	if(e.keyCode==39){
		cameras[0].right=false;
	}
	if(e.keyCode==40){
		cameras[0].down=false;
	}
	if(e.keyCode==107){
		cameras[0].in=false;
	}
	if(e.keyCode==109){
		cameras[0].out=false;
	}
}

var date = new Date();
var frame = 0;
var fps = 0;

window.onload = function(){
	var camera = new Camera();
}


var repeat = setInterval(function(){

	for ( var i in cameras ){
		cameras[i].move();
	}
	if(elements.length>0){
		for ( var i in elements ){
			elements[i].update();
		}
	}
	frame++;
	if(date != Date()){
		date = Date();
		fps=frame;
		frame=0;
		console.log(fps);
		//document.getElementById('fps').textContent=fps;
	}
},17);