<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">

	*{
		margin:0;
		padding: 0;
		position: absolute;
	}
	html{
		font-size: calc(1vw + 1vh);
	}

</style>
</head>
<body>
	<script type="text/javascript">
		function vh(value){
			var vh=window.innerWidth/100;
			return 'result '+value/vh
		}
		function rect(elem){

			var bodyRect = document.body.getBoundingClientRect(),
			elemRect = elem.getBoundingClientRect();
			offset  = elemRect.top - bodyRect.top;
			var offsetY=offset;
			offset  = elemRect.left - bodyRect.left;
			var offsetX=offset;
			return{
				x: offsetX,
				y: offsetY,
				w: elemRect.width,
				h: elemRect.height
			}
		}
		var user ={}
		document.onmousemove=function(e){
			user.x=e.clientX;
			user.y=e.clientY;
		}
		document.onmousedown=function(e){
			user.down=true;
		}
		document.onmouseup=function(e){
			user.down=false;
		}
		var doms={};
		function createDom(tag,attributes,style,events){

			var dom=document.createElement(tag);

			for(var i in style){
				dom.style[i]=style[i];
			}

			for(var i in attributes){
				dom.setAttribute(i,attributes[i]);
			}

			for(var i in events){
				dom[i]=events[i];
			}

			if(doms[tag]==undefined){
				doms[tag]=[];
			}

			console.log(doms);
			dom.id=tag+doms[tag].length;
			doms[tag].push(dom);

			return dom;

		}

		Layout = function(){
			this.dom = createDom('section',{},
			{
				width:'100px',
				height:'100px',
				backgroundColor:'silver',
			},
			{});
			var panels=[];
			panels.push(createDom('div',{},
			{
				height:'1rem',
				width:'calc(100% - 2rem)',
				backgroundColor:'red',
				opacity:0,
				right:'1rem',
			},
			{
				onmouseover:function(e){
					this.style.opacity=1;
				},
				onmouseout:function(e){
					this.style.opacity=0;
				},
				onmousedown:function(e){
					if(this.repeat==undefined){
						this.style.backgroundColor='green';
						var el = this;
						this.repeatFunction=function(){
							if(user.x>rect(el).x && user.x<rect(el).x+rect(el).w){
								el.style.opacity=1;
								el.parentNode.style.marginTop=user.y-rect(el).h/2+'px';
							}else{
								el.style.opacity=0;
								clearInterval(el.repeat);
								el.style.backgroundColor='red';
								el.repeat=undefined;
							}
						}
						this.repeat = setInterval(this.repeatFunction,17);
					}
				},
				onmouseup:function(e){
					if(this.repeat!=undefined){
						clearInterval(this.repeat);
						this.style.backgroundColor='red';
						this.repeat=undefined;
					}
				}
			}
			));
			panels.push(createDom('div',{},
			{
				width:'1rem',
				height:'calc(100% - 2rem)',
				backgroundColor:'red',
				opacity:0,
				right:0,
				top:'1rem'
			},
			{
				onmouseover:function(e){
					this.style.opacity=1;
				},
				onmouseout:function(e){
					this.style.opacity=0;
				},
				onmousedown:function(e){
					if(this.repeat==undefined){
						this.style.backgroundColor='green';
						var el = this;
						this.repeatFunction=function(){
							if(user.y>rect(el).y && user.y<rect(el).y+rect(el).h){
								el.style.opacity=1;
								el.parentNode.style.width=user.x+rect(el).w/2-rect(el.parentNode).x+'px';
							}else{
								el.style.opacity=0;
								clearInterval(el.repeat);
								el.style.backgroundColor='red';
								el.repeat=undefined;
							}
						}
						this.repeat = setInterval(this.repeatFunction,17);
					}
				},
				onmouseup:function(e){
					if(this.repeat!=undefined){
						clearInterval(this.repeat);
						this.style.backgroundColor='red';
						this.repeat=undefined;
					}
				}
			}
			));
			panels.push(createDom('div',{},
			{
				width:'1rem',
				height:'calc(100% - 2rem)',
				backgroundColor:'red',
				opacity:0,
				top:'1rem'
			},
			{	
				onmouseover:function(e){
					this.style.opacity=1;
				},
				onmouseout:function(e){
					this.style.opacity=0;
				},
				onmousedown:function(e){
					if(this.repeat==undefined){
						this.style.backgroundColor='green';
						var el = this;
						this.repeatFunction=function(){
							if(user.y>rect(el).y && user.y<rect(el).y+rect(el).h){
								el.style.opacity=1;
								el.parentNode.style.width=rect(el.parentNode).w+rect(el.parentNode).x-(user.x-rect(el).w/2)+'px';
								el.parentNode.style.marginLeft=user.x-rect(el).w/2+'px';
							}else{
								el.style.opacity=0;
								clearInterval(el.repeat);
								el.style.backgroundColor='red';
								el.repeat=undefined;
							}
						}
						this.repeat = setInterval(this.repeatFunction,17);
					}
				},
				onmouseup:function(e){
					if(this.repeat!=undefined){
						clearInterval(this.repeat);
						this.style.backgroundColor='red';
						this.repeat=undefined;
					}
				}
			}
			));
			for(var i in panels){
				this.dom.appendChild(panels[i]);
			}
			document.body.appendChild(this.dom);
		}

		var layout = new Layout();
		console.log(layout);
/*
onmousedown:function(el,e){
					if(el.repeat==undefined){
							el.style.backgroundColor='green';
						el.repeatFunction=function(){
								el.parentNode.style.width=user.x+el.getBoundingClientRect().width/2+'px';
								//console.log('repeat',window);
						}
						el.repeat = setInterval(el.repeatFunction,17);
					}else{
							clearInterval(el.repeat);
							el.style.backgroundColor='red';
							el.repeat=undefined;
					}
				}
				*/
			</script>
		</body>
		</html>