var spaces=[];

Space = function(options){

	this.points = {
		all:[]
	}
	this.cameras = {
		all:[]
	}
	this.paths = {
		all:[]
	}

	this.dom = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	document.body.appendChild(this.dom);
	spaces.push(this);

}



////////////////////////////////////////////////////////////////////////////////////////////////

Point = function(options){

	this.x = check('x',options,0);
	this.y = check('y',options,0);
	this.z = check('z',options,1);
	this.space = check('space',options,spaces[0]);
	this.target = check('target',options,null);
	if(this.target!=null){
		this.target.push(this);
	}
	this.space.points.all.push(this);
	return this;

}

function newPoint(options){
console.log(options.target);
	var target =  check('target',options,null);
	for(var i in target){
		if(options.x == target[i].x && options.y == target[i].y){
			console.log('point allready exist');
			return;
		}
	}
	var point = new Point(options);

}

////////////////////////////////////////////////////////////////////////////////////////////////

var em=32;

var Camera = function(options){
	this.position = new Point({x:check('x',options,0),y:check('y',options,0),z:check('z',options,1)});
	this.target = new Point({x:check('targetX',options,window.innerWidth/2),y:check('targetY',options,window.innerHeight/2)});
	this.speed = check('speed',options,8);
	this.traveling={
		left : false,
		up : false,
		right : false,
		down : false,
		front : false,
		back : false
	};
	this.grid = {
		value : check('grid',options,em),
		transfer : check('gridTransfer',options,em),
		ratio : check('ratio',options,1)
	};
	this.layer = 0;
	this.space = check('space',options,spaces[0]);
	this.space.cameras.all.push(this);
}

Camera.prototype.interface = function(e,bool){
	var keys=[37,38,39,40,107,109];
	var traveling=['left','up','right','down','front','back'];
	for(var i in keys){
		if(e.keyCode==keys[i]){
			this.traveling[traveling[i]]=bool;

		}
	}
}

Camera.prototype.move = function(what){
	var cam = this;
	var conduct = {
		left:function(){cam.position.x=cam.position.x-(1*cam.speed)/cam.position.z},
		up:function(){cam.position.y=cam.position.y-(1*cam.speed)/cam.position.z},
		right:function(){cam.position.x=cam.position.x+(1*cam.speed)/cam.position.z},
		down:function(){cam.position.y=cam.position.y+(1*cam.speed)/cam.position.z},
		front:function(){cam.position.z=cam.position.z*(1+cam.speed/350)},
		back:function(){cam.position.z=cam.position.z/(1+cam.speed/350)},
	};
	for (var i in this.traveling){
		if(this.traveling[i])
		{
			conduct[i]();
		}
	}
	var base=this.grid.transfer*this.position.z;
	if(parseInt(base)>this.grid.value*2){
		this.grid.transfer=this.grid.transfer/2;
		this.layer++;
	}
	if(parseInt(base)<this.grid.value){
		this.grid.transfer=this.grid.transfer*2;
		this.layer--;
	}
	this.update(what);
}

Camera.prototype.tune = function(axis,value){
	return parseInt((this.target[axis]-(this.target[axis]-value+this.position[axis]*this.position.z)/this.position.z)/this.grid.transfer)*this.grid.transfer;
}

Camera.prototype.update = function(what){

	for(var i in what){
		what[i].transfer={
			view:20*(what[i].z-this.position.z)/this.position.z,
			x:this.target['x']-(this.target['x']-what[i].x-this.position.x)*this.position.z,
			y:this.target['y']-(this.target['y']-what[i].y-this.position.y)*this.position.z,
			w:what[i].w*this.position.z,
			h:what[i].h*this.position.z,
			opacity:(what[i].w*this.position.z-what[i].w*what[i].z)/2500
		}
		//console.log(what[i].transfer);
		if(what[i].transfer.x + what[i].transfer.w > what[i].transfer.view && what[i].transfer.y + what[i].transfer.h > what[i].transfer.view && what[i].transfer.x < window.innerWidth-what[i].transfer.view && what[i].transfer.y < window.innerHeight-what[i].transfer.view && what[i].transfer.opacity<1){
			
			if(document.getElementById(what[i].name)==null){
				what[i].parent.appendChild(what[i].dom);
			}
			updateAttributes(what[i].dom,{
				viewBox:'0 0 '+what[i].transfer.w/this.position.z+' '+what[i].transfer.h/this.position.z,
			})
			updateCSS(what[i].dom,{
				visibility:'visible',
				marginLeft:what[i].transfer.x+'px',
				marginTop:what[i].transfer.y+'px',
				width:what[i].transfer.w+'px',
				height:what[i].transfer.h+'px',
				opacity:1-what[i].transfer.opacity,
				//fontSize:1/(what/*.position*/.z/this.position.position.z)+'em',
			});

		}else{
			console.log(what[i].name);
			if(document.getElementById(what[i].name)!=null){
				what[i].parent.removeChild(what[i].dom);
			}
		}
	}
}



////////////////////////////////////////////////////////////////////////////////////////////////

var paths=[];
Path = function(options){
	this.name = check('name',options,'path'+paths.length);
	this.dom=document.createElementNS('http://www.w3.org/2000/svg', 'path');
	this.points= check('points',options,[]);
	this.x=smaller(check('x',this.points));
	this.y=smaller(check('y',this.points));
	this.w=bigger(check('x',this.points))-smaller(check('x',this.points));
	this.h=bigger(check('y',this.points))-smaller(check('y',this.points));
	this.z=smaller(check('z',this.points));
	this.parent = check('parent',options,spaces[0].dom);
	this.path='M';
	this.drawPath();
	updateCSS(this.dom,{
		marginLeft:this.x+'px',
		marginTop:this.y+'px',
		width:this.w+'px',
		height:this.h+'px',

	})
	this.dom.id=this.name;
	this.parent.appendChild(this.dom);
	paths.push(this);
	//console.log(this);
}

Path.prototype.drawPath = function(options){
	this.path='M';
	for(var i = 0 ; i<this.points.length-1; i++){
		this.path=this.path+' '+(this.points[i].x-this.x)+' ';
		this.path=(i != this.points.length-1)?this.path+(this.points[i].y-this.y)+' L':this.path+this.points[i].y;
	}
	updateAttributes(this.dom,{
		d:this.path,
		stroke:'rgb('+random(0,255)+','+random(0,255)+','+random(0,255)+')',
		fill:'none'
	})
	updateCSS(this.dom,{
		strokeWidth:1/this.z,
		strokeLinecap:'round',
		strokeLinejoin:'round',
	})
}

////////////////////////////////////////////////////////////////////////////////////////////////