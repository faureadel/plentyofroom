var points = {all:[]};

Point = function(options){

	this.x = check('x',options,0);
	this.y = check('y',options,0);
	this.z = check('z',options,1);
	this.target = check('target',options,null);
	if(this.target!=null){
		this.target.push(this);
	}
	points.all.push(this);
	return this;
}

function newPoint(options){
	var target =  check('target',options,points);
	for(var i in target){

		if(options.x == target[i].x && options.y == target[i].y && target.length-i<3){
			console.log('point allready exist');
			return;
		}
	}
	var point = new Point(options);
	return point;
}

var paths=[];
Path = function(options){
	this.name = check('name',options,'path'+paths.length);

	this.points= check('points',options,[]);
	this.strokeWidth=check('strokeWidth',options,20);
	this.parent = check('parent',options,document.body.getElementsByTagName('main')[0]);
	this.path='M';
	this.dom=document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	this.dom.style.zIndex=paths.length;
	this.dom.id=this.name;
	paths.push(this);
	console.log(this);
}

Path.prototype.drawPath = function(options){
	if(this.points.length<2){
		return;
	}
	this.z=bigger(check('z',this.points));
	this.x=smaller(check('x',this.points))-this.strokeWidth/this.z;
	this.y=smaller(check('y',this.points))-this.strokeWidth/this.z;
	this.w=bigger(check('x',this.points))-smaller(check('x',this.points))+this.strokeWidth*2/this.z;
	this.h=bigger(check('y',this.points))-smaller(check('y',this.points))+this.strokeWidth*2/this.z;
	this.path='M';
	for(var i = 0 ; i<this.points.length; i++){
		this.path=this.path+' '+(this.points[i].x-this.x)+' ';
		this.path=(i != this.points.length)?this.path+(this.points[i].y-this.y)+' L':this.path+this.points[i].y;
	}
	var paths = this.dom.getElementsByTagName('path');
	for(var i=0 ; i<paths.length;i++){
		this.dom.removeChild(paths[i]);
	}
	var dom = document.createElementNS('http://www.w3.org/2000/svg', 'path');
	if(this.path.charAt(this.path.length-1)=='L'){
		var expectedPath='';
		for(var i = 0 ; i < this.path.length-1 ; i++){
			expectedPath=expectedPath+this.path.charAt(i);
		}
	}
	this.path=expectedPath;
	var stroke=check('stroke',options,'rgb('+random(0,255)+','+random(0,255)+','+random(0,255)+')');

	updateAttributes(dom,{
		d:this.path,
		stroke:stroke,
		fill:'rgb('+random(0,255)+','+random(0,255)+','+random(0,255)+')'
	})
	updateCSS(dom,{
		marginLeft:this.x+'px',
		marginTop:this.y+'px',
		width:this.w+'px',
		height:this.h+'px',
		strokeWidth:this.strokeWidth/this.z,
		strokeLinecap:'round',
		strokeLinejoin:'round',
	})
	this.dom.appendChild(dom);
}