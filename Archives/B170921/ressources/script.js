
document.onkeydown = function(e){
	cameras[0].interface(e,true);
	if(e.keyCode==32){
		paths[paths.length-1].drawPath({stroke:'none'});
		var path = new Path({});
	}
}

document.onkeyup = function(e){
	cameras[0].interface(e,false);
}

document.onmousewheel =function(e){
	scroll=scroll+2;
	if(e.wheelDelta>0){
		wheelDelta=1;
	}
	if(e.wheelDelta<0){
		wheelDelta=-1;
	}

}

var user={
	mousedown:false,
	mousedrag:false,
	mousemove:false
}

document.onmousedown =function(e){
	
	user.mousedown=true;
	if(event.button==2){
		document.oncontextmenu =new Function("return false;");
		rightClick=true;
	}else{
		document.oncontextmenu =new Function("return true;");
		var point = newPoint({
			x:camera.tune('x',e.clientX),
			y:camera.tune('y',e.clientY),
			z:camera.position.z,
			target:paths[paths.length-1].points
		});
		paths[paths.length-1].drawPath();
	}
}

document.onmouseup = function(e){
	user.mousedown=false;

	

}
var pointers=[];
var Pointer = function(i){
	this.i=i,
	this.x=0,
	this.y=0,
	this.z=1,
	this.h=10,
	this.w=10,
	this.dom=document.createElement('div');
	this.parent=undefined;
	this.dom.className='pointer';
	pointers.push(this);
}
for (var i = 0; i < 10; i++) {
	var pointer = new Pointer(i);
}


document.onmousemove = function(e){
	var x=0;
	for (var i = 0 ; i <pointers.length ; i++){
		x++;
		if(x>1){
			pointers[i].x=camera.tune('x',e.clientX)-camera.tune('x',camera.grid.value*pointers[i].i/2);
			x=0;
		}else{
			pointers[i].x=camera.tune('x',e.clientX)+camera.tune('x',camera.grid.value*pointers[i].i/2);
		}
		
		pointers[i].y=camera.tune('y',e.clientY);
	}
	if(user.mousedown==true){
		
	}
	if(user.mousedown && user.button==2){
		camera.position.x=camera.position.x-((user.downX-e.clientX)/33)/camera.position.z;
		camera.position.y=camera.position.y-((user.downY-e.clientY)/33)/camera.position.z;
	}
}
var scroll=0;
var wheelDelta=0;
window.onload = function(){
	for (var i in pointers){
		pointers[i].parent=document.body;
	}
	var path = new Path({});
	var fps = 0;
	var date = new Date();
	var frame = 0;
	console.log(window);
	var repeat = setInterval(function(){
		if(scroll>0){
			if(wheelDelta>0){
				cameras[0].traveling.front=true;
			}
			if(wheelDelta<0){
				cameras[0].traveling.back=true;
			}
			scroll--;

		}else{
			if(wheelDelta>0){
				cameras[0].traveling.front=false;
			}
			if(wheelDelta<0){
				cameras[0].traveling.back=false;
			}
			wheelDelta=0;
		}
		for (var i in pointers){
			pointers[i].z=camera.position.z;
			pointers[i].h=camera.grid.value/camera.position.z;
			pointers[i].w=camera.grid.value/camera.position.z;
		}
		for ( var i in cameras ){
			cameras[i].move(pointers);
			cameras[i].move(paths);
		}

		frame++;
		if(date != Date()){
			date = Date();
			fps=frame;
			frame=0;
			//console.log(fps+' fps');
		}

	},17);
}