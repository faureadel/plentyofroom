var cameras = [];

var Camera = function(options){
	this.x = check(options,'x',0);
	this.y = check(options,'y',0);
	this.z = check(options,'z',1);
	this.centreX = check(options,'centreX',window.innerWidth/2);
	this.centreY = check(options,'centreY',window.innerHeight/2);
	this.left = false;
	this.up = false;
	this.right = false;
	this.down = false;
	this.in = false;
	this.out = false;
	this.vitesse = 8;
	cameras.push(this);
}
Camera.prototype.move = function(){
	if(this.up==true){
		this.y=this.y-(1*this.vitesse)/this.z;
	}
	if(this.right==true){
		this.x=this.x+(1*this.vitesse)/this.z;
	}
	if(this.left==true){
		this.x=this.x-(1*this.vitesse)/this.z;
	}
	if(this.down==true){
		this.y=this.y+(1*this.vitesse)/this.z;
	}
	if(this.in==true){
		this.z=this.z*(1+this.vitesse/350);

		var base=baseGrid*this.z;
		if(parseInt(base)>91){
			baseGrid=baseGrid/2;
		}
		console.log(parseInt(base));
		
		drawGrid(base);
	}
	if(this.out==true){
		this.z=this.z/(1+this.vitesse/350);
		drawGrid(baseGrid*this.z);
	}
}

var elements={
	all:[],
	interface:[]
};

var Element = function(options){
	this.name = check(options,'name','el'+elements.length);
	this.camera = check(options,'camera',cameras[0]);
	this.x = this.camera.centreX-(this.camera.centreX-check(options,'x',0)+this.camera.x*this.camera.z)/this.camera.z;
	this.y = this.camera.centreY-(this.camera.centreY-check(options,'y',0)+this.camera.y*this.camera.z)/this.camera.z;
	this.w = check(options,'w',0)/this.camera.z;
	this.h = check(options,'h',0)/this.camera.z;
	this.z = check(options,'z',1);
	this.rect = check(options,'rect',{});
	this.parent = check(options,'parent',document.body);
	this.dom = document.createElement(check(options,'dom','div'));
	this.dom.style.zIndex=elements.length;
	this.dom.id=this.name;
	elements.push(this);
}

Element.prototype.update = function(){
	if(document.getElementById(this.name)==null){
		this.parent.appendChild(this.dom);
	}

	this.rect={
		view:20*(this.z-this.camera.z)/this.camera.z,
		x:this.camera.centreX-(this.camera.centreX-this.x-this.camera.x)*this.camera.z,
		y:this.camera.centreY-(this.camera.centreY-this.y-this.camera.y)*this.camera.z,
		w:this.w*this.camera.z,
		h:this.h*this.camera.z,
		opacity:(this.w*this.camera.z-this.w*this.z)/2500
	}

	if(this.rect.x + this.rect.w > this.rect.view && this.rect.y + this.rect.h > this.rect.view && this.rect.x < window.innerWidth-this.rect.view && this.rect.y < window.innerHeight-this.rect.view && this.rect.opacity<1){
		updateCSS(this.dom,{
			marginLeft:this.rect.x+'px',
			marginTop:this.rect.y+'px',
			width:this.rect.w+'px',
			height:this.rect.h+'px',
			opacity:1-this.rect.opacity,
			fontSize:1/(this.z/this.camera.z)+'em',
		});
	}else{
		this.parent.removeChild(this.dom);
	}
}

document.onmouseup = function(e){
	//var element = new Element({x:e.clientX,y:e.clientY,z:cameras[0].z});
}

document.onkeydown = function(e){
	//console.log(e.keyCode);
	if(e.keyCode==37){
		cameras[0].left=true;
	}
	if(e.keyCode==38){
		cameras[0].up=true;
	}
	if(e.keyCode==39){
		cameras[0].right=true;
	}
	if(e.keyCode==40){
		cameras[0].down=true;
	}
	if(e.keyCode==107){
		cameras[0].in=true;
	}
	if(e.keyCode==109){
		cameras[0].out=true;
	}
}

document.onkeyup = function(e){
	if(e.keyCode==37){
		cameras[0].left=false;
	}
	if(e.keyCode==38){
		cameras[0].up=false;
	}
	if(e.keyCode==39){
		cameras[0].right=false;
	}
	if(e.keyCode==40){
		cameras[0].down=false;
	}
	if(e.keyCode==107){
		cameras[0].in=false;
	}
	if(e.keyCode==109){
		cameras[0].out=false;
	}
}

var date = new Date();
var frame = 0;
var fps = 0;

Point = function(options){
	this.x = (options != undefined && options.x != undefined)?options.x:0;
	this.y = (options != undefined && options.y != undefined)?options.y:0;
	this.q = (options != undefined && options.q != undefined)?options.q:false;
}

Path = function(options){
	this.pts=(options != undefined && options.pts != undefined)?options.pts:[];
	this.pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
}

Path.prototype.drawPath = function(options){
	var path='M';
	for(var i in this.pts){
		path=path+' '+this.pts[i].x+' ';
		path=(i != this.pts.length-1)?path+this.pts[i].y+' L':path+this.pts[i].y;
	}
}

function drawGrid(base){
	
	var grid = document.getElementById('grid');
	if(grid!=null){
		grid.parentNode.removeChild(grid);
	}
	var svgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	svgElement.id='grid';
	for (var i = 0; i < window.innerHeight/base ; i++) {
		var pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
		var path='M';
		var pts=[];
		var pt = new Point({x:0,y:base*i});
		pts.push(pt);
		var pt = new Point({x:window.innerWidth,y:base*i});
		pts.push(pt);
		for(var j in pts){
			path=path+' '+pts[j].x+' ';
			path=(j != pts.length-1)?path+pts[j].y+' L':path+pts[j].y;
		}
		updateAttributes(pathElement,{
			d:path,
			fill:'none',
			stroke:'grey'
		});
		updateCSS(pathElement,{
			strokeWidth:'1'
		});
		
		svgElement.appendChild(pathElement);
	}
	for (var i = 0; i < window.innerWidth/base ; i++) {
		var pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
		var path='M';
		var pts=[];
		var pt = new Point({x:0,y:base*i});
		pts.push(pt);
		var pt = new Point({x:base*i,y:window.innerHeight});
		pts.push(pt);
		for(var j in pts){
			path=path+' '+pts[j].x+' ';
			path=(j != pts.length-1)?path+pts[j].y+' L':path+pts[j].y;
		}
		updateAttributes(pathElement,{
			d:path,
			fill:'none',
			stroke:'grey'
		});
		updateCSS(pathElement,{
			strokeWidth:'1'
		});
		
		svgElement.appendChild(pathElement);
	}
	updateCSS(svgElement,{
		width:'100%',
		height:'100%',
	});
	document.body.appendChild(svgElement);
}

var baseGrid = 48;
window.onload = function(){
	var camera = new Camera();
	
	
}


var repeat = setInterval(function(){

	for ( var i in cameras ){
		cameras[i].move();
	}
	if(elements.length>0){
		for ( var i in elements ){
			elements[i].update();
		}
	}
	frame++;
	if(date != Date()){
		date = Date();
		fps=frame;
		frame=0;
		console.log(fps+' fps');
		//document.getElementById('fps').textContent=fps;
	}
},17);