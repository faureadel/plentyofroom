
//EXPERIMENTAL//

//'M 0 0 L 100 0 L 0 100 q 100 50 100 100 '

sketchElement = function(options){
	this.space = (options != undefined && options.space != undefined)?options.space:spaces[0];
	this.name = (options != undefined && options.name != undefined)?options.name:this.space.elements.length;
	this.space.elements.push(this);
	this.x = (options != undefined && options.x != undefined)?options.x:0;
	this.y = (options != undefined && options.y != undefined)?options.y:0;
	this.w = (options != undefined && options.w != undefined)?options.w:630;
	this.h = (options != undefined && options.h != undefined)?options.h:891;
	this.gridValue = (options != undefined && options.gridValue != undefined)?options.gridValue:64;
	this.svgElement = (options != undefined && options.svgElement != undefined)?options.svgElement: new svgElement({x:this.x,y:this.y,w:this.w,h:this.h});
	this.gridElement = (options != undefined && options.gridElement != undefined)?options.gridElement: new svgElement({x:this.x,y:this.y,w:this.w,h:this.h,gridValue:this.gridValue});
	this.gridElement.svg.setAttribute('gridValue',this.gridValue);
	this.gridElement.svg.addEventListener('mousemove',function(e){
		var x=e.clientX;
		var y=e.clientY;
		var pointer = document.getElementById('pointer');
		pointer.style.marginLeft=parseInt((x-rect(this).x)/this.getAttribute('gridValue'))*this.getAttribute('gridValue')+rect(this).x-rect(pointer).w/2+'px';
		pointer.style.marginTop=parseInt((y-rect(this).y)/this.getAttribute('gridValue'))*this.getAttribute('gridValue')+rect(this).y-rect(pointer).h/2+'px';
	})
	this.gridElement.drawGrid();

}

svgElement = function(options){
	this.space = (options != undefined && options.space != undefined)?options.space:spaces[0];
	this.name = (options != undefined && options.name != undefined)?options.name:this.space.elements.length;
	this.space.elements.push(this);
	this.x = (options != undefined && options.x != undefined)?options.x:0;
	this.y = (options != undefined && options.y != undefined)?options.y:0;
	this.w = (options != undefined && options.w != undefined)?options.w:630;
	this.h = (options != undefined && options.h != undefined)?options.h:891;
	this.gridValue = (options != undefined && options.gridValue != undefined)?options.gridValue:64;
	this.background = (options != undefined && options.background != undefined)?options.background:'none';
	this.pathElements = (options != undefined && options.pathElements != undefined)?options.pathElements:[];
	this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	this.svg.style.marginLeft=this.x+'px';
	this.svg.style.marginTop=this.y+'px';
	this.svg.style.backgroundColor=this.background;
	this.svg.style.width=this.w+'px';
	this.svg.style.height=this.h+'px';
	this.svg.id=this.name;
	this.svg.addEventListener('click',function(){


		var pointer = document.getElementById('pointer');
		var svgEl = spaces[0].elements[this.id];
		var x=rect(pointer).x-svgEl.x+rect(pointer).w/2;
		var y=rect(pointer).y-svgEl.y+rect(pointer).w/2;
		var pt = new point({x:x,y:y});
		svgEl.pathElements[svgEl.pathElements.length-1].pts.push(pt);
		svgEl.pathElements[svgEl.pathElements.length-1].stroke='black';
		svgEl.drawPaths();

	})
	document.body.appendChild(this.svg);
}

svgElement.prototype.drawPaths = function(options){
	var paths = this.svg.getElementsByTagName('path');
	if(paths.length>0){
		for(var i = 0 ; i < paths.length ; i++ ){
			paths[i].parentNode.removeChild(paths[i]);
		}
	}
	for(var i = 0 ; i < this.pathElements.length ; i++ ){
		this.pathElements[i].drawPath();
		this.svg.appendChild(this.pathElements[i].path);
	}
}

svgElement.prototype.drawGrid = function(options){
	console.log(this,"drawingGrid");
	var a = this.pathElements.length;
	this.pathElements=[];
	for( var i = 0 ; i<a ; i ++){
		this.drawPaths();
	}

	for (var i = 0; i < this.h/this.gridValue ; i++) {
		var pts=[];
		var pt = new point({x:0,y:this.gridValue*i});
		pts.push(pt);
		var pt = new point({x:this.w,y:this.gridValue*i});
		pts.push(pt);
		var pathEl = new pathElement({weight:1,svg:this,pts:pts,stroke:'silver'});

	}

	for (var i = 0; i < this.w/this.gridValue ; i++) {
		var pts=[];
		var pt = new point({x:this.gridValue*i,y:0});
		pts.push(pt);
		var pt = new point({x:this.gridValue*i,y:this.h});
		pts.push(pt);
		var pathEl = new pathElement({weight:1,svg:this,pts:pts,stroke:'silver'});
	}
	var pathEl = new pathElement({weight:1,svg:this,stroke:'black'});
	this.drawPaths();

}

pathElement = function(options){
	this.svg = (options != undefined && options.svg != undefined)?options.svg:undefined;
	this.name = (options != undefined && options.name != undefined)?options.name:this.svg.pathElements.length;
	this.svg.pathElements.push(this);
	this.svgElement=(options != undefined && options.svgElement != undefined)?options.svgElement:undefined;
	this.pts=(options != undefined && options.pts != undefined)?options.pts:[];
	this.fill = (options != undefined && options.fill != undefined)?options.fill:'none';
	this.stroke = (options != undefined && options.stroke != undefined)?options.stroke:'black';
	this.weight = (options != undefined && options.weight != undefined)?options.weight:1;
	this.path=(options != undefined && options.path != undefined)?options.path:document.createElementNS('http://www.w3.org/2000/svg', 'path');
	this.path.setAttribute('index',this.name);
	this.path_str='M';
	
}

pathElement.prototype.changeAttribute = function(options){
	this.fill = (options != undefined && options.fill != undefined)?options.fill:'none';
	this.stroke = (options != undefined && options.stroke != undefined)?options.stroke:'black';
	this.weight = (options != undefined && options.weight != undefined)?options.weight:1;
}

pathElement.prototype.drawPath = function(options){
	this.path_str='M';
	for(var i in this.pts){
		//console.log(this.pts[i]);
		this.path_str=this.path_str+' '+this.pts[i].x+' ';
		this.path_str=(i != this.pts.length-1)?this.path_str+this.pts[i].y+' L':this.path_str+this.pts[i].y;
	}
	if(this.path_str!='M'){
		this.path.setAttribute('d',this.path_str);
	}else{
		this.path.setAttribute('d','M 0 0');
	}
	this.path.setAttribute('fill',this.fill);
	this.path.setAttribute('stroke',this.stroke);
	this.path.setAttribute('stroke-width',this.weight);

}

point = function(options){
	this.x = (options != undefined && options.x != undefined)?options.x:0;
	this.y = (options != undefined && options.y != undefined)?options.y:0;
	this.q = (options != undefined && options.q != undefined)?options.q:false;
}





//NORMAL//

var spaces=[];

// por est un espace

space = function(options){

	// on peux mettre des choses dans cet espace
	this.elements = (options != undefined && options.elements != undefined)?options.elements:[];
	// il peux potentielement y avoir plusieurs espaces auquels acceder, ceux-ci ont donc un identifiant pour les différencier
	this.name = (options != undefined && options.name != undefined)?options.name:spaces.length;

	spaces.push(this);

}

eval_onload(
	function(){
		//EXPERIMENTAL//
		var world = new space();
		//var sketch = new sketchElement({gridValue:16});
		console.log(world);
//pointer//

var pointer = document.createElement('div');
pointer.id='pointer';
pointer.style.width='10px';
pointer.style.height='10px';
pointer.style.position='absolute';
pointer.style.zIndex='999';
pointer.style.backgroundColor='red';
pointer.style.borderRadius='50%';
pointer.style.pointerEvents='none';
document.body.appendChild(pointer);

//preview
var preview = document.createElement('div');
preview.id='preview';
preview.style.position='absolute';
preview.style.zIndex='997';
preview.style.backgroundColor='aliceblue';
preview.setAttribute('mouseIsOver',false);
preview.addEventListener('mouseover',function(e){
	this.setAttribute('mouseIsOver',true);
	preview.style.backgroundColor='pink';
})
preview.addEventListener('mouseout',function(e){
	this.setAttribute('mouseIsOver',false);
	preview.style.backgroundColor='aliceblue';
})
document.body.appendChild(preview);

		//NORMAL//
		
		
		
	}
	);

var sketchCreationState;
var mouse={x:0,y:0,lastX:0,lastY:0,upX:0,upY:0,downX:0,downY:0,isMoving:false,isDown:false};
document.onmousemove = function(e){
	mouse.x=e.clientX;
	mouse.y=e.clientY;
}
document.onmousedown = function(e){
	mouse.downX=e.clientX;
	mouse.downY=e.clientY;
	mouse.isDown=true;
	console.log('mouseIsDown');
}
document.onmouseup = function(e){
	mouse.upX=e.clientX;
	mouse.upY=e.clientY;
	mouse.isDown=false;
	console.log('mouseIsUp');
	if(document.getElementsByTagName('svg').length==0){
		var preview=document.getElementById('preview');
		if(preview.getAttribute('mouseIsOver')=='false'){
			var buttons=preview.getElementsByTagName('button');
			for(var i = 0 ; i < buttons.length ; i ++ ){
				buttons[i].parentNode.removeChild(buttons[i]);
			}
			var button = document.createElement('button');
			button.textContent='done';
			button.addEventListener('click',function(e){
				var preview=document.getElementById('preview');
				var sketch = new sketchElement({x:rect(preview).x,y:rect(preview).y,w:rect(preview).w,h:rect(preview).h});
				document.getElementById('preview').parentNode.removeChild(document.getElementById('preview'));
			})
			preview.appendChild(button);
		}
	}else{
	}
}
repeat(function(){

	if(mouse.x!=mouse.lastX || mouse.y!=mouse.lastY ){
		mouse.lastX=mouse.x;
		mouse.lastY=mouse.y;
		mouse.isMoving=true;
		console.log('mouseIsMoving');
	}else{
		mouse.isMoving=false;
		
	}

	if(mouse.isDown==true && mouse.isMoving == true && document.getElementsByTagName('svg').length==0){
		var preview=document.getElementById('preview');
		if(preview.getAttribute('mouseIsOver')=='false'){
			if(mouse.x>mouse.downX){
				preview.style.marginLeft=mouse.downX+'px';
				preview.style.width=mouse.x-rect(preview).x+'px';
			}else{
				preview.style.marginLeft=mouse.x+'px';
				preview.style.width=mouse.downX-mouse.x+'px';
			}
			if(mouse.y>mouse.downY){
				preview.style.marginTop=mouse.downY+'px';
				preview.style.height=mouse.y-rect(preview).y+'px';
			}else{
				preview.style.marginTop=mouse.y+'px';
				preview.style.height=mouse.downY-mouse.y+'px';
			}
		}
	}


});
