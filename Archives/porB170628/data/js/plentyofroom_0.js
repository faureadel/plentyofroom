elements=[];

element = function(options){
	if(options != undefined){
		for(var i in options){
			this[i]=options[i];
		}
	}
	elements.push(this);
}

element.prototype.defineFonction = function(fonction) {
	this.fonctions[fonctions.length] = fonction;
}

element.prototype.evalFonctions = function(){
	for(var i in this.fonctions){
		this.fonctions[i](this);
	}
}

element.prototype.updateDomElement = function(options){

	if(document.getElementById(this.domElement.id) == undefined){
		document.body.appendChild(this.domElement);
	}
	if(options != undefined){
		if(options.attributes != undefined){
			for(var i in options.attributes){
				this.domElement.setAttribute(i,options.attributes[i]);
			}
		}
		if(options.style != undefined){
			for(var i in options.style){
				this.domElement.style[i]=options.style[i];
			}
		}

	}
}

var mouse={x:0,y:0,lastX:0,lastY:0,upX:0,upY:0,downX:0,downY:0,isMoving:false,isDown:false};

document.onmousemove = function(e){
	mouse.x=e.clientX;
	mouse.y=e.clientY;
}
document.onmousedown = function(e){
	mouse.downX=e.clientX;
	mouse.downY=e.clientY;
	mouse.isDown=true;
}
document.onmouseup = function(e){
	mouse.upX=e.clientX;
	mouse.upY=e.clientY;
	mouse.isDown=false;
}

var client = new element({
	name:'client',
	mouse:mouse,
})

var mainGrid = new element({
	name:'mainGrid',
	x:0,
	y:0,
	width:undefined,
	height:undefined,
	gridValue:64,
	domElement:document.createElementNS('http://www.w3.org/2000/svg', 'svg'),
	fonctions:[
	function(el){
		if(client.mouse.isMoving==true){
			if(el.width!=window.innerWidth){
				el.width=window.innerWidth;
				el.draw(el.height/el.gridValue,0,el.gridValue,el.width,el.gridValue,0.25,'silver');
			}
			if(el.height!=window.innerHeight){
				el.height=window.innerHeight;
				
			}
		}
	}
	]
});

window.onload = function(){


}

repeat(function(){

	if(mouse.x!=mouse.lastX || mouse.y!=mouse.lastY ){
		mouse.lastX=mouse.x;
		mouse.lastY=mouse.y;
		mouse.isMoving=true;
		//console.log('mouseIsMoving');
	}else{
		mouse.isMoving=false;
		
	}

	for(var i in elements){
		elements[i].evalFonctions();
	}
})