(function(global){


	Frame = function(position){
    	/**
		* "the frame, champ, point of vue, on which depends what we see from the Surface, beeing also a layer to place FrameElements"
        The ?second? highest DOM element managing the transformation of the elements on the Surface.
        ? @property DOMElement {DOM Object}
        */
        this.DOMElement = document.createElement("nav");
        this.DOMElement.style.position = "absolute";
        this.DOMElement.style.width = width || "100%";
        this.DOMElement.style.height = height || "100%";

        document.body.appendChild(this.DOMElement);

        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.width = width || "100%";
        this.height = height || "100%";
        this.centerX = 0;
        this.centerY = 0;

    };

    /**
    * FrameElement is an element that can be attached to the Frame. After its creation, it can be used to contain Text, Image, Video or Audio that will be displayed over the Surface like for example dynamic or non-dynamic information or tools.
    * @class FrameElement
    *
    */ 

    FrameElement = function(position,size){

    	this.DOMElement = null;

    	this.name = name;

    	/*<! definir les modalités de structuration de l'element >*/
    	this.style = null;
    	this.hyper_text = null;
    	this.graphic = null;
    	/*</!>*/

    	/**
		* i is for initial position "non modifiable after being a data"
		* d is for displayed, the "actual perceptible position" depending on the frame position
		*/

		this.x = 0;
		this.y = 0;
		this.width = 0;
		this.height = 0;
		this.opacity = 1;
		this.visible = 1;

		/*<! definir les modalités de structuration de l'element >*/
		this.haveStyle = false;
		this.containHyper_text = false;
		this.containGraphic = false;
		/*</!>*/

	}

	Surface = function(){

		/**
		* "An ambiguously deep surface"
        The underlying DOM element on which SurfaceElements can be fixed.
        ? @property DOMElement {DOM Object}
        */

        this.DOMElement = document.createElement("main");
        this.DOMElement.style.position = "absolute";
        document.html.appendChild(this.DOMElement);

		/**
        The list of all SurfaceElement attached to this Surface
        ? @property elements {Array}
        */

        this.elements = [];
    };
    /*<?>
	Surface.goToHomeEvent = function(){
        console.log("click",this);
        window.location = this.url;
    };
    </?>*/

	/**
    * Define the backgroundColor of the Surface
    ? @method setBackgroundColor
    ? @param {Number || CSS string} color number for red OR number to repeat on the 3 color channels OR CSS string
    ? @param {Number} color number for green (optional)
    ? @param {Number} color number for blue (optional)
    */

    Surface.prototype.setBackgroundColor = function(r,g,b,a){

    	if(arguments.length === 1){
    		if(typeof r === "string"){
    			this.DOMElement.style.backgroundColor = r;
    		}else if(typeof r === "number"){
    			this.DOMElement.style.backgroundColor =
    			"rgb("+r+","+r+","+r+")";
    		}
    	}else{
    		if(arguments.length === 3){
    			this.DOMElement.style.backgroundColor =
    			"rgb("+r+","+g+","+b+")";
    		}
    		if(arguments.length === 4){
    			this.DOMElement.style.backgroundColor =
    			"rgba("+r+","+g+","+b+","+a+")";
    		}
    	}
    };

    /**
    * Define the backgroundImage of the Surface
    */

    Surface.prototype.setBackgroundImage = function(url,repeat,attachment,position,size){

    	if(arguments.length === 1){
    		this.DOMElement.style.backgroundImage = "url("+url+")";
    	}else{
    		this.DOMElement.style.backgroundImage = "url("+url+")";
    		this.DOMElement.style.backgroundRepeat = repeat;
    		this.DOMElement.style.backgroundAttachement = attachment;
    		this.DOMElement.style.backgroundPosition = position;
    		this.DOMElement.style.backgroundSize = size;
    	}
    };

    /**
    * Add a SurfaceElement to the Surface.
    ? @method addElement
    ? @param {SurfaceElement} element
    */

    Surface.prototype.addElement = function(el){
    	this.elements.push(el);
    	this.DOMElement.appendChild(el.DOMElement);
    };

    /**
    * Returns the SurfaceElement of this name
    ? @method getElementByName
    ? @param {String} name
    ? @return {SurfaceElement} element which has this name
    */

    Surface.prototype.getElementByName = function(name){

    	for(var i=0; i<this.elements.length; i++){
    		if(this.elements[i].name === name){
    			return this.elements[i];
    		}
    	}

    };

	/**
	* "a element on the Surface, their set define the visual environment"
    * SurfaceElement is an element that can be attached to the Surface. Before its creation, it content has to be define : Text, Image, Video, Audio... The SurfaceElement holds a media and its transformation is managed by the Frame according to its position in the Surface.
    ? @class SurfaceElement
    *
    */    

    SurfaceElement = function (){

    	this.DOMElement = null;

    	this.name = null;
    	this.birth_date = null;
    	this.author = null;

    	/*<! definir les modalités de structuration de l'element >*/
    	this.style = null;
    	this.hyper_text = null;
    	this.graphic = null;
    	/*</!>*/

    	/**
		* i is for initial position "non modifiable after being a data"
		* d is for displayed, the "actual perceptible position" depending on the frame position
		* w is for width
		* h is for heigth
		*/

		this.i_x = 0;
		this.i_y = 0;
		this.i_z = 0;
		this.i_w = 0;
		this.i_h = 0;
		this.d_x = 0;
		this.d_y = 0;
		this.d_z = 0;
		this.d_w = 0;
		this.d_h = 0;

		/*<!>*/
    	/*this.rot = 0; like an axis ?
    	OR
    	this.rotationX = 0;
    	this.rotationY = 0;
    	*/
    	/*</!>*/

    	

    	this.opacity = 1;
    	this.visible = 1;

    	/*<! definir les modalités de structuration de l'element >*/
    	this.haveStyle = false;
    	this.containHyper_text = false;
    	this.containGraphic = false;
    	/*</!>*/

    };

    /**
    * apply the transformations dues to the Frame deplacement
    */

    SurfaceElement.prototype.updateDisplay = function(){
    	this.d_x = Frame.centerX-(Frame.centerX-this.i_x-Frame.x)*Frame.z;
    	this.d_y = Frame.centerY-(Frame.centerY-this.i_y-Frame.y)*Frame.z;
    	this.d_z = this.i_z*Frame.z;
    	this.d_w = this.i_w*Frame.z;
    	this.d_h = this.i_h*Frame.z;
    };

    Text_editor = function(){

    }

    Preview = function(value){

    }

    Station = function (get,send){

    }


})(this);