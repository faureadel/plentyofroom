
var spaceElements=[];

spaceElement = function(options){
	this.name = (options != undefined && options.name != undefined)?options.name:spaceElements.length;
	this.x = (options != undefined && options.x != undefined)?options.x:options.gridValue;
	this.y = (options != undefined && options.y != undefined)?options.y:options.gridValue;
	this.z = (options != undefined && options.z != undefined)?options.z:1;
	this.w = (options != undefined && options.w != undefined)?options.w:(options.gridValue*10)*0.7;
	this.h = (options != undefined && options.h != undefined)?options.h:options.gridValue*10;
	this.domElements = (options != undefined && options.domElements != undefined)?options.domElements:[];
	spaceElements.push(this);
	console.log(this.name,'new spaceElement','DONE');
}

spaceElement.prototype.updateDomElement = function(options){
	var domElement = document.getElementById(options.domElement.id);
	if(domElement != undefined){
		domElement.parentNode.removeChild(domElement);
	}
	if(options != undefined){
		if(options.attributes != undefined){
			for(var i in options.attributes){
				options.domElement.setAttribute(i,options.attributes[i]);
			}
		}
		if(options.style != undefined){
			for(var i in options.style){
				options.domElement.style[i]=options.style[i];
			}
		}
		options.parentNode.appendChild(options.domElement);
	}
	console.log(this.name,'updateDomElement','DONE');
}

point = function(options){
	this.x = (options != undefined && options.x != undefined)?options.x:0;
	this.y = (options != undefined && options.y != undefined)?options.y:0;
	this.q = (options != undefined && options.q != undefined)?options.q:false;
	console.log('new point','DONE');
}

path = function(options){
	this.pts=(options != undefined && options.pts != undefined)?options.pts:[];
	this.pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
	console.log('new path','DONE');
}

pathElement.prototype.drawPath = function(options){
	var path='M';
	for(var i in this.pts){
			path=path+' '+this.pts[i].x+' ';
			path=(i != this.pts.length-1)?path+this.pts[i].y+' L':path+this.pts[i].y;
		}
}

spaceElement.prototype.drawGrid = function(options){
	for (var i = 0; i < this.h/options.gridValue ; i++) {
		var pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
		this.domElements[2].push(pathElement);
		var path='M';
		var pts=[];
		var pt = new point({x:0,y:options.gridValue*i});
		pts.push(pt);
		var pt = new point({x:this.w,y:options.gridValue*i});
		pts.push(pt);
		for(var j in pts){
			path=path+' '+pts[j].x+' ';
			path=(j != pts.length-1)?path+pts[j].y+' L':path+pts[j].y;
		}
		this.updateDomElement({domElement:pathElement,parentNode:this.domElements[0],attributes:{d:path,fill:'none',stroke:'white'},style:{strokeWidth:'1'}});
	}
	for (var i = 0; i < this.w/options.gridValue ; i++) {
		var pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
		this.domElements[2].push(pathElement);
		var path='M';
		var pts=[];
		var pt = new point({x:options.gridValue*i,y:0});
		pts.push(pt);
		var pt = new point({x:options.gridValue*i,y:this.h});
		pts.push(pt);
		for(var j in pts){
			path=path+' '+pts[j].x+' ';
			path=(j != pts.length-1)?path+pts[j].y+' L':path+pts[j].y;
		}
		this.updateDomElement({domElement:pathElement,parentNode:this.domElements[0],attributes:{d:path,fill:'none',stroke:'white'},style:{strokeWidth:'1'}});
	}
	console.log(this.name,'drawGrid','DONE');
}

function createSpaceElement(){
	var el = new spaceElement({gridValue:64});
	var svgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	svgElement.id = el.name+'svg';
	el.domElements.push(svgElement);
	var divElement = document.createElement('div');
	divElement.id = el.name+'div';
	el.domElements.push(divElement);
	var pathElements = [];
	el.domElements.push(pathElements);
	el.updateDomElement({domElement:svgElement,parentNode:divElement,attributes:{viewBox:'0 0 '+el.w+' '+el.h},style:{backgroundColor:'lavender'}});
	el.updateDomElement({domElement:divElement,parentNode:document.body,style:{marginLeft:el.x+'px',marginTop:el.y+'px',width:el.w+'px',height:el.h+'px',backgroundColor:'red'}});
	return el;
}


eval_onload(
	function(){
		var el = createSpaceElement();
		el.drawGrid({gridValue:64});
		

		console.log(spaceElements);
		console.log(' eval_onload ','DONE');
	}
	);