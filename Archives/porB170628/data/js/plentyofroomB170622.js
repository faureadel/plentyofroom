var grid=50;
//EXPERIMENTAL//
document.onmousemove = function(e){
	var x=e.clientX;
	var y=e.clientY;

	var pointer = document.getElementById('pointer');
	pointer.style.marginLeft=parseInt(x/grid)*grid+'px';
	pointer.style.marginTop=parseInt(y/grid)*grid+'px';



}

//'M 0 0 L 100 0 L 0 100 q 100 50 100 100 '
svgElement = function(options){
	this.space = (options != undefined && options.space != undefined)?options.space:spaces[0];
	this.name = (options != undefined && options.name != undefined)?options.name:this.space.elements.length;
	this.x = (options != undefined && options.x != undefined)?options.x:0;
	this.y = (options != undefined && options.y != undefined)?options.y:0;
	this.w = (options != undefined && options.w != undefined)?options.w:300;
	this.h = (options != undefined && options.h != undefined)?options.h:200;
	this.background = (options != undefined && options.background != undefined)?options.background:'none';
	this.pathElements = (options != undefined && options.pathElements != undefined)?options.pathElements:[];
	this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	this.svg.style.marginLeft=this.x+'px';
	this.svg.style.marginTop=this.y+'px';
	this.svg.style.backgroundColor=this.background;
	this.svg.setAttribute('width',this.w);
	this.svg.setAttribute('height',this.h);
	this.svg.setAttribute('viewBox','0 0 '+this.w+' '+this.h);
	this.space.elements.push(this);
}

svgElement.prototype.drawPaths = function(options){

	var paths = this.svg.getElementsByTagName('path');
	if(paths.length>0){
		for(var i = 0 ; i < paths.length ; i++ ){
			paths[i].parentNode.removeChild(paths[i]);
		}
	}
	for(var i = 0 ; i < this.pathElements.length ; i++ ){
		this.pathElements[i].drawPath();
		this.svg.appendChild(this.pathElements[i].path);
	}

}

pathElement = function(options){
	this.svg = (options != undefined && options.svg != undefined)?options.svg:undefined;
	this.name = (options != undefined && options.name != undefined)?options.name:this.svg.pathElements.length;
	this.svgElement=(options != undefined && options.svgElement != undefined)?options.svgElement:undefined;
	this.pts=(options != undefined && options.pts != undefined)?options.pts:[];
	this.fill = (options != undefined && options.fill != undefined)?options.fill:'none';
	this.stroke = (options != undefined && options.stroke != undefined)?options.stroke:'black';
	this.weight = (options != undefined && options.weight != undefined)?options.weight:1;
	this.path=(options != undefined && options.path != undefined)?options.path:document.createElementNS('http://www.w3.org/2000/svg', 'path');
	this.path.setAttribute('index',this.name);
	this.path_str='M';
	this.svg.pathElements.push(this);
}

pathElement.prototype.drawPath = function(options){
	this.path_str='M';
	for(var i in this.pts){
		//console.log(this.pts[i]);
		this.path_str=this.path_str+' '+this.pts[i].x+' ';
		this.path_str=(i != this.pts.length-1)?this.path_str+this.pts[i].y+' L':this.path_str+this.pts[i].y;
	}
	if(this.path_str!='M'){
		this.path.setAttribute('d',this.path_str);
	}else{
		this.path.setAttribute('d','M 0 0');
	}
	this.path.setAttribute('fill',this.fill);
	this.path.setAttribute('stroke',this.stroke);
	this.path.setAttribute('stroke-width',this.weight);
}

point = function(options){
	this.x = (options != undefined && options.x != undefined)?options.x:0;
	this.y = (options != undefined && options.y != undefined)?options.y:0;
	this.q = (options != undefined && options.q != undefined)?options.q:false;
}





//NORMAL//

function default_ressource(el) {

	ressource = document.createElement('div');
	ressource.id = el.name;
	ressource.style.width='200px';
	ressource.style.height='300px';
	ressource.style.backgroundImage='blue';
	return ressource;

}

function default_sight_ressource(el) {

	ressource = document.createElement('section');
	ressource.id = el.name;
	ressource.style.width='800px';
	ressource.style.height='600px';
	ressource.style.outline='solid';
	ressource.style.outlineWidth='1px';
	return ressource;

}

var spaces=[];

// por est un espace

space = function(options){

	// on peux mettre des choses dans cet espace
	this.elements = (options != undefined && options.elements != undefined)?options.elements:[];
	// il peux potentielement y avoir plusieurs espaces auquels acceder, ceux-ci ont donc un identifiant pour les différencier
	this.name = (options != undefined && options.name != undefined)?options.name:spaces.length;
	// pour observer les elements contenus dans cet espace, un point ou plusieurs points de vus sont à définir
	this.sights = (options != undefined && options.elements != undefined)?options.elements:[];
	
	spaces.push(this);

}

// ces elements ont des propriétées spatiales 

element = function(options){
	// ces éléments appartiennent donc à un espace
	this.space = (options != undefined && options.space != undefined)?options.space:spaces[0];
	// ils sont également associés à un point de vue spécifique de l'espace auquel ils appartiennent servant de référent pour leur affichage
	this.sight = (options != undefined && options.sight != undefined)?options.sight:this.space.sights[0];
	// ils ont une position horizontale, verticale
	this.x = (options != undefined && options.x != undefined)?options.x:undefined;
	this.y = (options != undefined && options.y != undefined)?options.y:undefined;
	// et également une position en profondeur
	this.z = (options != undefined && options.z != undefined)?options.z:undefined;
	// ils occupent une surface sur cet espace, en largeur et en hauteur
	this.w = (options != undefined && options.w != undefined)?options.w:undefined;
	this.h = (options != undefined && options.h != undefined)?options.h:undefined;
	// Ces élements ont pour rôle de contenir une ressource
	// ces ressources sont des objets intégrés par l'utilisateur, par défaut il s'agit d'élements DOM
	
	// l'élément peut être dans le champs de vision ou non, il est visible par défaut
	this.visibility = (options != undefined && options.visibility != undefined)?options.visibility:1;
	//SIGHT PROPRETIES
	if(options != undefined && options.sight != undefined){
		this.aiming = (options != undefined && options.aiming != undefined)?options.aiming:{x:this.w/2,y:this.h/2};
	// il peux potentielement y avoir plusieurs éléments, ceux-ci ont donc un identifiant pour les différencier
	this.name = (options != undefined && options.name != undefined)?options.name:this.space.name+'si'+this.space.sights.length;
	this.ressource = (options != undefined && options.ressource != undefined)?options.ressource:default_sight_ressource(this);
	this.space.sights.push(this);
}else{
	this.name = (options != undefined && options.name != undefined)?options.name:this.space.name+'e'+this.space.elements.length;
	this.ressource = (options != undefined && options.ressource != undefined)?options.ressource:default_ressource(this);
}
}

// ils ont également pour rôle d'afficher ces ressources dans l'espace auquel ils appartiennent en fonction du point
element.prototype.displayRessource = function(options){
	// l'objectif est que la ressource s'adapte aux propriétés spaciales de l'élément auquelle elle appartient
	// si cette ressource est un element DOM l'operation nécéssite des fonctions spécifiques, je ne sais pas encore comment je vais faire si la ressource est autre ou si elle sera systématiquement convertie en dom
	console.log(this.name+'.ressource ? instanceof HTMLElement =',this.ressource instanceof HTMLElement == true );
	//if(this.ressource instanceof HTMLElement == true ){
		//si la ressource html n'est pas dans le body, elle est ajoutée, au chargement par exemple ou si elle n'était pas dans le champs de vision dans un soucis d'optimisation par exemple
		if(document.getElementById(this.ressource.id)==null){
			document.body.appendChild(this.ressource);
		}else{
		// si elle est déjà présente dans le body il faut que ses caractéristiques spatiales correspondent à l'élément plentyofroom auquelle elle appartient
		// cela vaut aussi pour son aspect (transparence par exemple)
		this.ressource.style.opacity=this.visibility;
		this.ressource.style.marginLeft=this.x+this.sight.x+'px';
		this.ressource.style.marginTop=this.y+this.sight.y+'px';
			// pour que la ressource s'affiche la faire correspondre avec son element por ne suffit pas il faut également prendre le point de vue (sight) de l'utilisateur sur l'espace auquelle son élément appartient
			this.ressource.style.height=this.h*sight.z+'px';
			this.ressource.style.width=this.w*sight.z+'px';
		}

	//}
}

eval_onload(
	function(){
		//EXPERIMENTAL//
		// grid
		//pointer//

		var pointer = document.createElement('div');
		pointer.id='pointer';
		pointer.style.width='10px';
		pointer.style.height='10px';
		pointer.style.position='absolute';
		pointer.style.zIndex='999';
		pointer.style.backgroundColor='red';
		pointer.style.borderRadius='50%';
		pointer.style.pointerEvents='none';
		document.body.appendChild(pointer);



		//drawer
		var world = new space();
		var svgEl = new svgElement({space:world,w:'100%',h:'100%'});
		var svgGrid = new svgElement({space:world,w:'100%',h:'100%'});
		svgEl.svg.addEventListener('click',function(e){
			var pointer = document.getElementById('pointer');
			var x=rect(pointer).x-svgEl.x;
			var y=rect(pointer).y-svgEl.y;
			var pt = new point({x:x,y:y});
			svgEl.pathElements[svgEl.pathElements.length-1].pts.push(pt);
			svgEl.drawPaths();
			document.getElementById('path_str').textContent=svgEl.pathElements[svgEl.pathElements.length-1].path_str;
			console.log(world);
		});

		document.body.appendChild(svgGrid.svg);
		document.body.appendChild(svgEl.svg);
		var section = document.createElement('section');
		var span = document.createElement('span');
		span.textContent='x';
		section.appendChild(span);
		var input = document.createElement('input');
		input.id='xInput';
		section.appendChild(input);
		var span = document.createElement('span');
		span.textContent='y';
		section.appendChild(span);
		var input = document.createElement('input');
		input.id='yInput';
		section.appendChild(input);
		var button = document.createElement('button');
		button.textContent='add point';
		button.addEventListener('click',function(){
			var pt = new point({x:document.getElementById('xInput').value,y:document.getElementById('yInput').value});
			svgEl.pathElements[svgEl.pathElements.length-1].pts.push(pt);
			svgEl.drawPaths();
			document.getElementById('path_str').textContent=svgEl.pathElements[svgEl.pathElements.length-1].path_str;
		})
		section.appendChild(button);


		var button = document.createElement('button');
		button.textContent='new path';
		button.addEventListener('click',function(){
			var pathEl = new pathElement({weight:2,svg:svgEl});
			pathEl.path.addEventListener('mouseover',function(e){
				console.log(this.getAttribute('index'));
				this.setAttribute('stroke','pink');
			});
			pathEl.path.addEventListener('mouseout',function(e){
				console.log(this.getAttribute('index'));
				this.setAttribute('stroke',svgEl.pathElements[this.getAttribute('index')].stroke);
			});
		})
		section.appendChild(button);


		var button = document.createElement('button');
		button.textContent='clear path';
		button.addEventListener('click',function(){
			svgEl.pathElements[svgEl.pathElements.length-1].pts=[];
			svgEl.drawPaths();
			document.getElementById('path_str').textContent=svgEl.path_str;
		})
		section.appendChild(button);
		var button = document.createElement('button');
		button.textContent='clear svg';
		button.addEventListener('click',function(){
			var a = svgEl.pathElements.length;
			svgEl.pathElements=[];
			for( var i = 0 ; i<a ; i ++){
				svgEl.drawPaths();
			}
		})
		section.appendChild(button);
		var input = document.createElement('input');
		input.id='gridInput';
		section.appendChild(input);
		var button = document.createElement('button');
		button.textContent='grid value';
		button.addEventListener('click',function(){
			grid = document.getElementById('gridInput').value;
			document.getElementById('grid_value').textContent=grid;
			

			var a = svgGrid.pathElements.length;
			svgGrid.pathElements=[];
			for( var i = 0 ; i<a ; i ++){
				svgGrid.drawPaths();
			}
			

			for (var i = 0; i < parseInt(rect(svgGrid.svg).h/grid) ; i++) {
				var pts=[];
				var pt = new point({x:0,y:grid*i});
				pts.push(pt);
				var pt = new point({x:1920,y:grid*i});
				pts.push(pt);
				var pathEl = new pathElement({weight:0.5,svg:svgGrid,pts:pts,stroke:'grey'});
			}

			for (var i = 0; i < parseInt(rect(svgGrid.svg).w/grid) ; i++) {
				var pts=[];
				var pt = new point({x:grid*i,y:0});
				pts.push(pt);
				var pt = new point({x:grid*i,y:1920});
				pts.push(pt);
				var pathEl = new pathElement({weight:0.5,svg:svgGrid,pts:pts,stroke:'grey'});
			}

			svgGrid.drawPaths();
			
		})
		section.appendChild(button);
		var p = document.createElement('p');
		p.id='grid_value';
		section.appendChild(p);
		var p = document.createElement('p');
		p.id='path_str';
		section.appendChild(p);
		document.body.appendChild(section);
		//NORMAL//
		
		
		
	}
	);

repeat(function(){
//console.log('coucou');
});
