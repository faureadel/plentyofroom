

// por est un espace
space = function(options){
	// on peux mettre des choses dans cet espace
	this.elements = (options.elements != undefined)?options.elements:[];
	// il peux potentielement y avoir plusieurs espaces auquels acceder, ceux-ci ont donc un identifiant pour les différencier
	this.name = (options.name != undefined)?options.name:Math.random(0,100);
	// pour observer les elements contenus dans cet espace, un point ou plusieurs points de vus sont à définir
	this.sights= (options.elements != undefined)?options.elements:[];
}
// ces elements ont des propriétées spatiales 
element = function(options){
	// ces éléments appartiennent donc à un espace
	this.space = (options.space != undefined)?options.space:undefined;
	// ils ont une position horizontale, verticale
	this.x = (options.x != undefined)?options.x:undefined;
	this.y = (options.y != undefined)?options.y:undefined;
	// et également une position en profondeur
	this.z = (options.z != undefined)?options.z:undefined;
	// ils occupent une surface sur cet espace, en largeur et en hauteur
	this.w = (options.w != undefined)?options.w:undefined;
	this.h = (options.h != undefined)?options.h:undefined;
	// Ces élements ont pour rôle de contenir une ressource
	// ces ressources sont des objets intégrés par l'utilisateur, par défaut il s'agit d'élements DOM
	this.ressource = (options.ressource != undefined)?options.ressource:document.createElement('div');
	// l'élément peut être dans le champs de vision ou non, il est visible par défaut
	this.visibility = (options.visibility != undefined)?options.visibility:1;
}
// ils ont également pour rôle d'afficher ces ressources dans l'espace auquel ils appartiennent
element.prototype.displayRessource = function(options){
	// l'objectif est que la ressource s'adapte aux propriétés spaciales de l'élément auquelle elle appartient
	// si cette ressource est un element DOM l'operation nécéssite des fonctions spécifiques, je ne sais pas encore comment je vais faire si la ressource est autre ou si elle sera systématiquement convertie en dom
	if(this.ressource instanceof HTMLElement == true ){
		//si la ressource html n'est pas dans le body, elle est ajoutée, au chargement par exemple ou si elle n'était pas dans le champs de vision dans un soucis d'optimisation par exemple
		
		if(document.getElementById(this.ressource.id)==null){
			document.body.appendChild(this.haunted);
		}else{
		// si elle est déjà présente dans le body il faut que ses caractéristiques spatiales correspondent à l'élément plentyofroom auquelle elle appartient
		// cela vaut aussi pour son aspect (transparence par exemple)
		this.ressource.style.opacity=this.visibility;
		this.ressource.style.marginLeft=this.x+'px';
		this.ressource.style.marginTop=this.y+'px';
			// pour que la ressource s'affiche la faire correspondre avec son element por ne suffit pas il faut également prendre le point de vue (sight) de l'utilisateur sur l'espace auquelle son élément appartient
			this.ressource.style.height=this.h*sight.z+'px';
			this.ressource.style.width=this.w*sight.z+'px';
		}

	}
}
// le point de vue à également des propriétés spatiales
sight = function(options){
	this.space = (options.space != undefined)?options.space:undefined;
	this.x = (options.x != undefined)?options.x:undefined;
	this.y = (options.y != undefined)?options.y:undefined;
	this.z = (options.z != undefined)?options.z:undefined;
	this.w = (options.w != undefined)?options.w:undefined;
	this.h = (options.h != undefined)?options.h:undefined;
}
eval_onload(
	function(){
		console.log('caca');
	}
);