var user={
	name:undefined,
	statut:undefined,
	mouse:{
		moving:false,
		historyx:[],
		historyy:[],
		x:undefined,
		y:undefined,
		dragging:false,
		drag:{
			path:{
				x:[],
				y:[],
			}
		},
		pressing:false,
		down:{
			x:undefined,
			y:undefined,
			historyx:[],
			historyy:[],
		},
		up:{
			x:undefined,
			y:undefined,
			historyx:[],
			historyy:[],
		},
	},
	keys:{
		pressing:false,
		down:[],
		downhistory:[],
		up:undefined,
		uphistory:[],
	},
	window:{
		resizing:true,
		scrolling:true,
		html:document.getElementsByTagName("body")[0],
		x:undefined,
		y:undefined,
		w:undefined,
		h:undefined,
		scrolly:0,
	}
};
var commondraw={
	mousehistorylength:0,
	statutTimer:{
		reading:0,
		idle:0,
		writting:0,
		busy:0,
	}
}
start_onmousedown(use_onmousedown);
function use_onmousedown(){
	if(user.mouse.pressing==false){
		user.mouse.pressing=true;
		user.mouse.down.x=event.clientX;
		user.mouse.down.historyx.push(event.clientX);
		user.mouse.down.historyy.push(event.clientY);
		user.mouse.down.y=event.clientY;
		console.log("user.mouse.pressing",user.mouse.pressing);
		//console.log("user.mouse.down.x",user.mouse.down.x);
		//console.log("user.mouse.down.y",user.mouse.down.y);
		//console.log("user.mouse.down.historyx",user.mouse.down.historyx);
		//console.log("user.mouse.down.historyy",user.mouse.down.historyy);
	}
}
start_onmouseup(use_onmouseup);
function use_onmouseup(){
	if(user.mouse.pressing==true){
		user.mouse.pressing=false;
		user.mouse.up.x=event.clientX;
		user.mouse.up.historyx.push(event.clientX);
		user.mouse.up.historyy.push(event.clientY);
		user.mouse.up.y=event.clientY;
		//console.log("user.mouse.pressing",user.mouse.pressing);
		console.log("user.mouse.up.x",user.mouse.up.x);
		//console.log("user.mouse.up.y",user.mouse.up.y);
		//console.log("user.mouse.up.historyx",user.mouse.up.historyx);
		//console.log("user.mouse.up.historyy",user.mouse.up.historyy);
	}
}
start_onmousemove(use_onmousmove);
function use_onmousmove() { 
	if(user.mouse.moving==false){
		user.mouse.moving=true;
		console.log("user.mouse.moving",user.mouse.moving);
	}
	user.mouse.historyx.push(event.clientX);
	user.mouse.historyy.push(event.clientY);
	user.mouse.x=event.clientX;
	user.mouse.y=event.clientY;
	//console.log("user.mouse.x",user.mouse.x);
	//console.log("user.mouse.y",user.mouse.y);
	//console.log("user.mouse.historyx",user.mouse.historyx);
	//console.log("user.mouse.historyy",user.mouse.historyy);
}
function onmousedrag(e){
	e.drag.path.x.push(e.x);
	e.drag.path.y.push(e.y);

}
start_onkeydown(use_onkeydown);
function use_onkeydown(e,key){
		e = e || window.event;
	var key = e.which || e.keyCode;
console.log("use_onkeydown");
	var keysdown="";
	for (var i = user.keys.down.length - 1; i >= 0; i--) {
		keysdown= keysdown+String(user.keys.down[i])+',';
	}
	if(keysdown.includes(key+',')==false){
		user.keys.down.push(key);
		console.log("keysdown",keysdown);
		console.log("user.keys.down",user.keys.down);
		user.keys.downhistory.push(key);
	}
}
start_onkeyup(use_onkeyup);
function use_onkeyup(e,key){
	e = e || window.event;
	var key = e.which || e.keyCode;
	var keysdown="";
	var newKeysDown=[];
	for (var i = user.keys.down.length - 1; i >= 0; i--) {
		if(key!=user.keys.down[i]){
			newKeysDown.push(user.keys.down[i]);
		}else{
			user.keys.up=key;
			console.log("user.keys.up",user.keys.up);
			user.keys.uphistory.push(key);
		}
	}
	user.keys.down=newKeysDown;
	//console.log("user.keys.down",user.keys.down);

}
start_ondraw(use_ondraw);
function use_ondraw(){
	if(window.scrollY!=user.window.scrolly){
		if(user.window.scrolling==false){
			user.window.scrolling=true;
			//console.log("user.window.scrolling",user.window.scrolling);
		}
	}else{
		if(user.window.scrolling==true){
			user.window.scrolling=false;
			//console.log("user.window.scrolling",user.window.scrolling);
		}
	}
	user.window.scrolly=window.scrollY;

	try{
		rectAttributes(user.window.html);
		if(parseFloat(user.window.html.getAttribute("w"))!=parseFloat(user.window.w) || parseFloat(user.window.html.getAttribute("h"))!=parseFloat(user.window.h)){

			if(user.window.resizing!=true){
				user.window.resizing=true;
				//console.log("user.window.resizing",user.window.resizing);
			}
		}
		if(parseFloat(user.window.html.getAttribute("w"))==parseFloat(user.window.w) && parseFloat(user.window.html.getAttribute("h"))==parseFloat(user.window.h)){
			if(user.window.resizing!=false){
				user.window.resizing=false;
				//console.log("user.window.resizing",user.window.resizing);
			}
		}
	}catch(e){
		console.log("error",e);
		user.window.html=document.getElementsByTagName("body")[0];
		console.log("user.window.html",user.window.html);
		rectAttributes(user.window.html);
		user.window.w=user.window.html.getAttribute("w");
		user.window.h=user.window.html.getAttribute("h");
	}
	if(user.window.w!=user.window.html.getAttribute("w")){
		user.window.w=user.window.html.getAttribute("w");
	}
	if(user.window.h!=user.window.html.getAttribute("h")){
		user.window.h=user.window.html.getAttribute("h");
	}
	if(user.mouse.pressing==false && user.mouse.moving==false && user.keys.pressing==false){

		if(commondraw.statutTimer.reading<125){
			if(user.statut!="reading"){
				user.statut="reading";
				//console.log("user.statut",user.statut);
			}
			commondraw.statutTimer.reading++;
		}else{
			if(user.statut!="idle"){
				user.statut="idle";
				//console.log("user.statut",user.statut);
			}
		}
		if(commondraw.statutTimer.writting!=0){
			commondraw.statutTimer.writting=0;
		}
	}else{
		if(commondraw.statutTimer.writting<125){
			if(user.statut!="writting"){
				user.statut="writting";
				//console.log("user.statut",user.statut);
			}
			commondraw.statutTimer.writting++;
		}else{
			if(user.statut!="busy"){
				user.statut="busy";
				//console.log("user.statut",user.statut);
			}
		}
		if(commondraw.statutTimer.reading!=0){
			commondraw.statutTimer.reading=0;
		}
	}


	if(user.keys.down.length==0){
		if(user.keys.pressing==true){
			user.keys.pressing=false;
			//console.log("user.keys.pressing",user.keys.pressing);
		}
	}else{
		if(user.keys.pressing==false){
			user.keys.pressing=true;
			//console.log("user.keys.pressing",user.keys.pressing);
		}
	}
	if(user.mouse.moving==true){
		if(user.mouse.historyx.length==commondraw.mousehistorylength){
			user.mouse.moving=false;
			user.mouse.historyx=[];
			user.mouse.historyy=[];
			//console.log("user.mouse.moving",user.mouse.moving);
		}else{
			commondraw.mousehistorylength=user.mouse.historyx.length;
		}
		if(user.mouse.pressing==true){
			onmousedrag(user.mouse);
			if(user.mouse.dragging==false){
				user.mouse.dragging=true;
				//console.log("user.mouse.dragging",user.mouse.dragging);
			}
		}
	}else{
		if(user.mouse.dragging==true){
			user.mouse.dragging=false;
			//console.log("user.mouse.dragging",user.mouse.dragging);
			
		}
		if(user.mouse.dragging==false){
			user.mouse.drag.path.x=[];
			user.mouse.drag.path.y=[];
		}
	}
	
}
start_onload(use_inittouch());
function use_inittouch(){
  var el = document.getElementsByTagName("body")[0];
  el.addEventListener("touchstart", handleStart, false);
  el.addEventListener("touchend", handleEnd, false);
 // el.addEventListener("touchcancel", handleCancel, false);
  el.addEventListener("touchmove", handleMove, false);
  //print("touch","initialized");
}

function handleStart(evt) {
  evt.preventDefault();
  //print("handleStart","touchstart.");
  var el = document.getElementsByTagName("body")[0];
  //var ctx = el.getContext("2d");
  var touches = evt.changedTouches;
        
  for (var i = 0; i < touches.length; i++) {
    //print("handleStart","touchstart:" + i + "...");
    ongoingTouches.push(copyTouch(touches[i]));
    //var color = colorForTouch(touches[i]);
    //ctx.beginPath();
    //ctx.arc(touches[i].pageX, touches[i].pageY, 4, 0, 2 * Math.PI, false);  // a circle at the start
    //ctx.fillStyle = color;
    //ctx.fill();
    //print("handleStart","touchstart:" + i + ".");
  }
}
function handleMove(evt) {
	
  evt.preventDefault();
  var el = document.getElementsByTagName("body")[0];

  //var ctx = el.getContext("2d");
  var touches = evt.changedTouches;
  if(touches.length==1){
  	window.scrollY=touches[0].pageY;
  	//print("window.scrollY",window.scrollY);
  	document.getElementById("toolbox").style.marginTop=window.scrollY+"px";
  	//print("document.getElementById(\"toolbox\").style.marginTop",document.getElementById("toolbox").style.marginTop);
  	//print("document.getElementById(\"toolbox\").marginLeft",document.getElementById("toolbox").style.marginLeft);
  }
  //print("handleMove touches.length",touches.length);
  /*
  for (var i = 0; i < touches.length; i++) {
    var idx = ongoingTouchIndexById(touches[i].identifier);
    if (idx >= 0) {
      //print("handleMove","continuing touch "+idx);
      //ctx.beginPath();
      //print("handleMove","ctx.moveTo(" + ongoingTouches[idx].pageX + ", " + ongoingTouches[idx].pageY + ");");
      //ctx.moveTo(ongoingTouches[idx].pageX, ongoingTouches[idx].pageY);
      //print("handleMove","ctx.lineTo(" + touches[i].pageX + ", " + touches[i].pageY + ");");
      //ctx.lineTo(touches[i].pageX, touches[i].pageY);
      //ctx.lineWidth = 4;
      //ctx.strokeStyle = color;
      //ctx.stroke();

      ongoingTouches.splice(idx, 1, copyTouch(touches[i]));  // swap in the new touch record
      //print("handleMove",".");
    } else {
      //print("handleMove","can't figure out which touch to continue");
    }
  }*/
}
function handleEnd(evt) {
  evt.preventDefault();
  //print("handleEnd","touchend");
  var el = document.getElementsByTagName("body")[0];
   //ctx = el.getContext("2d");
  var touches = evt.changedTouches;

  for (var i = 0; i < touches.length; i++) {
    //var color = colorForTouch(touches[i]);
    var idx = ongoingTouchIndexById(touches[i].identifier);

    if (idx >= 0) {
      //ctx.lineWidth = 4;
      //ctx.fillStyle = color;
      //ctx.beginPath();
      //ctx.moveTo(ongoingTouches[idx].pageX, ongoingTouches[idx].pageY);
      //ctx.lineTo(touches[i].pageX, touches[i].pageY);
      //ctx.fillRect(touches[i].pageX - 4, touches[i].pageY - 4, 8, 8);  // and a square at the end
      ongoingTouches.splice(idx, 1);  // remove it; we're done
    } else {
      //print("handleEnd","can't figure out which touch to end");
    }
  }
}