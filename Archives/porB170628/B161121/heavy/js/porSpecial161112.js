var initspecialDraw=setInterval(specialdraw, 25);

function specialdraw(){

}
function onscroll(){

}

function onresize(){

}

function load(name){
	$.ajax({ url: 'php/load.php',
		data: { name : name},
		type: 'GET',
		datatype : 'html',
		success: function(data) {

			var getting=document.getElementById("getting")
			if(update==''){
				update=data;
				getting.innerHTML+=update;
			}
			if(update<data){
				var content=data.substring(data.length,update.length);

				getting.innerHTML=content;
				update=data;
			}
			var elements=getting.getElementsByTagName("br");
			if(install==true && getting.innerHTML!=''){
				for (var i = 0; i <= elements.length - 1; i++) {
					createElement($(elements[i]).attr('tag'),"sketch",$(elements[i]).attr('d-px'),$(elements[i]).attr('d-py'),$(elements[i]).attr('d-ex'),$(elements[i]).attr('d-ey'),$(elements[i]).attr('d-ez'),$(elements[i]).attr('d-ew'),
						$(elements[i]).attr('d-eh'),$(elements[i]).attr('d-html'),$(elements[i]).attr('d-style'));
				}
				getting.innerHTML='';
				refreshPositions(256);
			}


		}
	});
}

function createElement(what,where,ptX,ptY,posX,posY,posZ,posW,posH,html,style){
     //console.log("createElement");
     var pathData=[];
     arrX.push(posX);
     arrY.push(posY);
     arrZ.push(posZ);
     arrW.push(posW);
     arrH.push(posH);
     arrHtml.push(html);
     //user Array
     TempArrX.push(posX);
     TempArrY.push(posY);
     TempArrZ.push(posZ);
     TempArrW.push(posW);
     TempArrH.push(posH);
     TempArrHtml.push(html);
     var toPrint =  html.includes("class=\"toPrint\"");
     if(what=="svg"){
     	var ptXarr = JSON.parse("[" + ptX + "]");
     	var ptYarr = JSON.parse("[" + ptY + "]");
     	for (var i = 0; i < ptXarr.length; i++) {
     		var point = {x:ptXarr[i]-posX,y:ptYarr[i]-posY};
     		pathData.push(point);
     	}
     	simpleData=simplify(pathData, 3, false);
     	var toPush={path:pathData};
     	paths.push(toPush);
		//This is the accessor function we talked about above
		var lineFunction = d3.line()
		.x(function(d) { return d.x; })
		.y(function(d) { return d.y; })
		.curve(d3.curveCatmullRom.alpha(0.5));
		//The SVG Container
		var svgContainer = d3.select("#sketch").append("svg")
		.attr("class", "input");
		//The line SVG Path we draw
		var lineGraph = svgContainer.append("path")
		.attr("class", "line")
		.attr("d", lineFunction(simpleData))
		.attr("stroke", "rgb(72,72,72)")
		.attr("stroke-width", posZ)
		.attr("stroke-linecap","round")
		.attr("fill", "none");
		// ** Update data section (Called from the onclick)
	}else{
		var toPush={path:undefined};
		paths.push(toPush);
		var element = document.createElement(what);
		if(install==false){
			element.style.color="tomato";
		}else{
			element.style.color="rgb(72,72,72)"
		}
		var img=html.includes("<img src=");
		if(img==false){
			element.style.cssText = style;
		}else{
			element.style.position="absolute";
		}
		element.innerHTML=html;
		element.className="input";
		if(what=='p'){
			element.setAttribute("type","texte");
		}
		document.getElementById(where).appendChild(element);

		if(toPrint==true){
			var printElement = document.createElement(what);
			printElement.innerHTML=html;
			printElement.className="output";
			printElement.style.fontSize=14.5+"pt";
			document.getElementById("print").appendChild(printElement);
		}

	}
}