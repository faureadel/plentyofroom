var initspecialDraw=setInterval(specialdraw, 25);

function specialdraw(){
	if(user.window.resizing==true){
		onresize();
	}
	if(user.window.scrolling==true){
		onscroll();
	}
}
function onscroll(){
	definemacrosw();
	definemacrosh();
	definemacrosx();
	definepadx();
	definepadh();
}
function definemacrosx(){
	var toolbox=document.getElementById("toolbox");
	var macros=document.getElementById("macros");
	rectAttributes(toolbox);
	rectAttributes(macros);
	macros.style.marginLeft=(parseInt(toolbox.getAttribute("w"))-parseInt(macros.getAttribute("w")))/2+"px";
	
	//console.log("document.getElementById(\"macros\").style.width",document.getElementById("macros").style.width);
	
}
function definepadx(){
	var toolbox=document.getElementById("toolbox");
	var pad=document.getElementById("pad");
	rectAttributes(toolbox);
	rectAttributes(pad);
	pad.style.marginLeft=(parseInt(toolbox.getAttribute("w"))-parseInt(pad.getAttribute("w")))/2+"px";
	
	//console.log("document.getElementById(\"macros\").style.width",document.getElementById("macros").style.width);
	
}
function definepadh(){
	var toolbox=document.getElementById("toolbox");
rectAttributes(toolbox);
	var macros=document.getElementById("macros");
	rectAttributes(macros);
	var pad=document.getElementById("pad");
	rectAttributes(pad);
	pad.style.height=(toolbox.getAttribute("h")-macros.getAttribute("h")) - user.window.w/35 - (user.window.h-user.window.scrolly)+"px";
	
	//console.log("document.getElementById(\"macros\").style.width",document.getElementById("macros").style.width);
	
}
function definemacrosw(){
	rectAttributes(document.getElementById("toolbox"));
	if(user.window.h/1.65+user.window.scrolly<user.window.w*0.9){
		document.getElementById("macros").style.width=user.window.h/1.65+user.window.scrolly+"px";
		if(user.window.h/1.65+user.window.scrolly<user.window.w*0.33){
		document.getElementById("macros").style.width=user.window.w*0.33+"px";
		}
	}else{
		document.getElementById("macros").style.width=user.window.w*0.9+"px";
	}
	
	//console.log("document.getElementById(\"macros\").style.width",document.getElementById("macros").style.width);
	
}
function definemacrosh(){
	rectAttributes(user.window.html);
	//console.log(user.window.html.getAttribute("h"));
	document.getElementById("toolbox").style.marginTop=user.window.html.getAttribute("h")+"px";
	var macro=document.getElementsByClassName("macro")[0];
	rectAttributes(document.getElementsByClassName("macro")[0]);
	document.getElementById("macros").style.height=macro.getAttribute("w")+"px";

}
function onresize(){
	definemacrosw();
	definemacrosh();
	definemacrosx();
	definepadx();
	definepadh();
}