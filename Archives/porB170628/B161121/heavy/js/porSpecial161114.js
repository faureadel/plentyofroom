/*
//element data
<br 
e_tag=undefined
e_id=undefined
e_class=undefined
e_html=undefined 
e_cssTxt=undefined 
e_x=undefined 
e_y=undefined 
e_z=undefined  
e_w=undefined 
e_h=undefined
p_x=[] 
p_y=[]
p_z=[] 
>
*/
var champ={element:document.getElementById("champ"),x:undefined,y:undefined,z:undefined,location:undefined};
var inputs={
	getting:{
		update:'',
		loading:false,
	},
	toDisplay:[],
	tolink:{
		from:[],
		to:[],
		tag:[],
	}
}
var statut={
	setup:undefined,
}
function initByUrl(){
	console.log("url",window.location.href);
	var url=window.location.href;
	if (url.includes("<x>") && url.includes("</x>")) {
		var initX = url.substring(url.indexOf("<x>") + 3, url.indexOf("</x>"));
		if(initX!=undefined){
			champ.x=parseFloat(initX);
		}
	}else{
		champ.x=0;
	}
	if (url.includes("<y>") && url.includes("</y>")) {
		var initY = url.substring(url.indexOf("<y>") + 3, url.indexOf("</y>"));
		champ.y=parseFloat(initY);
	}else{
		champ.y=0;
	}
	if (url.includes("<z>") && url.includes("</z>")) {
		var initZ = url.substring(url.indexOf("<z>") + 3, url.indexOf("</z>"));
		champ.z=parseFloat(initZ);
	}else{
		champ.z=0;
	}
	if (url.includes("<location>") && url.includes("</location>")) {
		var initlocation = url.substring(url.indexOf("<location>") + 10, url.indexOf("</location>"));
		champ.location=initlocation;
	}else{
		champ.location="index";
	}

}
function overAttribute(e){
	e.setAttribute("over",true);
}
function outAttribute(e){
	e.setAttribute("over",false);
}
function macrosStatut(e){
	if(e.getAttribute("statut")!="actif"){
		e.setAttribute("cssText",e.style.cssText);
		e.setAttribute("statut","actif");
		e.style.border="solid";
		e.style.borderColor="lavender";
		var efonction=e.getAttribute("fonction");
		console.log(macros);
	}else{
		e.setAttribute("statut","inactif");
		e.style.cssText=e.getAttribute("cssText");
	}
}
var macros={
	element:[],
	fonction:[],
	brush:[],
}
function preview(){
	var preview=document.getElementById("preview");
	preview.style.marginLeft=user.mouse.x+"px";
	preview.style.marginTop=user.mouse.y+window.scrollY+"px";
}
function writePreview(){
	var preview=document.getElementById("preview");
	if(document.getElementById("write").getAttribute("statut")=="actif"){
		if(document.getElementById("champ").getAttribute("over")=="true"){
			document.getElementById("champ").style.cursor="text";
		}else{
			document.getElementById("champ").style.cursor="auto";
		}
		var pad=document.getElementById("pad");
		pad.focus();
		preview.innerHTML=pad.value;
	}else{
		preview.innerHTML='';
	}
}
function write(){
	if(document.getElementById("write").getAttribute("statut")=="actif" && document.getElementById("champ").getAttribute("over")=="true" && String(document.getElementById("preview").innerHTML).length>0){
		var preview=document.getElementById("preview");
		var newElementCss="margin-left:"+rect(preview).x+"px;margin-top:"+(rect(preview).y-window.scrollY)+"px;";
		console.log(newElementCss);
		var newElement = createInput("div","input"+document.getElementsByClassName("input").length,"input",newElementCss,preview.innerHTML,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined).element;
		newElement.onclick = function() {
			linking(this);
		}
		document.getElementById("champ").appendChild(newElement);
	}
}
function linking(e){
	var pad=document.getElementById("pad");
	var preview=document.getElementById("preview");
	if(document.getElementById("link").getAttribute("statut")=="actif" ){
		if(inputs.tolink.to[inputs.tolink.to.length-1]==preview){
			inputs.tolink.to[inputs.tolink.to.length-1]=e;
			var lines=document.getElementsByClassName("line")
			for (var i = lines.length - 1; i >= 0; i--) {
				if(lines[i].id.includes("preview")){
					lines[i].parentNode.removeChild(lines[i]);
				}
			}
		}else{
			inputs.tolink.tag.push(pad.value);
			inputs.tolink.from.push(e);
			inputs.tolink.to.push(preview);
		}
		console.log(inputs);
	}
}
document.onmousedown=function(e){
	console.log("mousedown");
	write();
}
function initMacros(){
	var macros=document.getElementsByClassName("macro");
	for (var i = macros.length - 1; i >= 0; i--) {
		var macro=macros[i];
		macro.onclick = function() {
			macrosStatut(this);
		}
	}
}
window.onload = function ssetup(){
	initMacros();
	initByUrl();
	var initsDraw=setInterval(sdraw, 25);
	statut.setup="done";

	
	console.log("statut.setup",statut.setup);
}
function sdraw(){
	writePreview();
	preview();
	for (var i = inputs.tolink.from.length - 1; i >= 0; i--) {
		var div1=inputs.tolink.from[i];
		var div2=inputs.tolink.to[i];
		if(div1==div2){
			document.getElementById(div1+"-"+div2).parentNode.removeChild(document.getElementById(div1+"-"+div2));
		}else{
			link(div1, div2, "MediumPurple", 1,inputs.tolink.tag[i]);
		}

		
	}
	if(user.statut=="reading"){
		sonreading();
	}
	if(user.statut=="writing"){
		sonwriting();

	}
}
function sonreading(){
	//updateUrl();

}
function sonwriting(){

}
function sonscroll(){

}

function sonresize(){

}
function updateUrl(){
	if(champ.location!=undefined&&champ.x!=undefined&&champ.y!=undefined&&champ.z!=undefined){
		ChangeUrl("Plenty Of Room"+String(champ.location),"#<location>"+champ.location+"</location><x>"+champ.x+"</x><y>"+champ.y+"</y><z>"+champ.z+"</z>");
	}
}
function ChangeUrl(title, url) {
	if (typeof (history.pushState) != "undefined") {
		var obj = { Title: title, Url: url };
		history.pushState(obj, obj.Title, obj.Url);
	} else {
		alert("Browser does not support HTML5.");
	}
}
function displayInputs(){
	console.log("displaying inputs");
}
function createInput(e_tag,e_id,e_class,e_cssTxt,e_html,e_x,e_y,e_z,e_w,e_h,p_x,p_y,p_z){
	var newInput=document.createElement(e_tag);
	if(e_id!=undefined){
		newInput.id=e_id;
	}
	newInput.className=e_class;
	newInput.style.cssText=e_cssTxt;
	newInput.innerHTML=e_html;
	newInput.setAttribute("e_x",e_x);
	newInput.setAttribute("e_y",e_y);
	newInput.setAttribute("e_z",e_z);
	newInput.setAttribute("e_w",e_w);
	newInput.setAttribute("e_h",e_h);
	newInput.setAttribute("e_w",e_w);
	newInput.setAttribute("p_x",e_h);
	newInput.setAttribute("p_y",e_h);
	newInput.setAttribute("p_z",e_h);
	return{
		element: newInput
	}
}
function link( div2,div1, color, thickness, tag) {
	var idCheck = document.getElementById(div1.id+'-'+div2.id);
    // bottom right
    var x1 = rect(div1).x;
    var y1 = rect(div1).y-window.scrollY;
    // top right
    var x2 = rect(div2).x;
    var y2 = rect(div2).y-window.scrollY;
    // distance
    var length = Math.sqrt(((x2-x1) * (x2-x1)) + ((y2-y1) * (y2-y1)));
    // center
    var cx = ((x1 + x2) / 2) - (length / 2);
    var cy = ((y1 + y2) / 2) - (thickness / 2);
    // angle
    var angle = Math.atan2((y1-y2),(x1-x2))*(180/Math.PI);
    var lineCss ="text-align:center;padding:0px; margin:0px; height:" + thickness + "px; background-color:" + color + "; line-height:1px; position:absolute; left:" + cx + "px; top:" + cy + "px; width:" + length + "px; -moz-transform:rotate(" + angle + "deg); -webkit-transform:rotate(" + angle + "deg); -o-transform:rotate(" + angle + "deg); -ms-transform:rotate(" + angle + "deg); transform:rotate(" + angle + "deg)";
    
    if(idCheck==null){
    	console.log("idCheck = null");
    // make hr
    var line = createInput("div",div1.id+'-'+div2.id,"line",lineCss,"<div class=\"tag\"><font style=\"font-size:12pt;color:white;background-color:"+color+"\">"+tag+"</div>",undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined).element;
    document.getElementById("champ").appendChild(line);
}else{
	if(lineCss!=idCheck.style.cssText){
		idCheck.style.cssText=lineCss;
	}
}
}

