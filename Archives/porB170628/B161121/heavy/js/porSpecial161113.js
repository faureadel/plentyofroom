/*
//element data
<br 
e_tag=undefined
e_id=undefined
e_class=undefined
e_html=undefined 
e_cssTxt=undefined 
e_x=undefined 
e_y=undefined 
e_z=undefined  
e_w=undefined 
e_h=undefined
p_x=[] 
p_y=[]
p_z=[] 
>
*/
var champ={element:document.getElementById("champ"),x:undefined,y:undefined,z:undefined,location:undefined};
var inputs={
	getting:{
		update:'',
		loading:false,
	},
	toDisplay:[],
}
var statut={
	setup:undefined,
}
function initByUrl(){
	console.log("url",window.location.href);
	var url=window.location.href;
	if (url.includes("<x>") && url.includes("</x>")) {
		var initX = url.substring(url.indexOf("<x>") + 3, url.indexOf("</x>"));
		if(initX!=undefined){
			champ.x=parseFloat(initX);
		}
	}else{
		champ.x=0;
	}
	if (url.includes("<y>") && url.includes("</y>")) {
		var initY = url.substring(url.indexOf("<y>") + 3, url.indexOf("</y>"));
		champ.y=parseFloat(initY);
	}else{
		champ.y=0;
	}
	if (url.includes("<z>") && url.includes("</z>")) {
		var initZ = url.substring(url.indexOf("<z>") + 3, url.indexOf("</z>"));
		champ.z=parseFloat(initZ);
	}else{
		champ.z=0;
	}
	if (url.includes("<location>") && url.includes("</location>")) {
		var initlocation = url.substring(url.indexOf("<location>") + 10, url.indexOf("</location>"));
		champ.location=initlocation;
	}else{
		champ.location="index";
	}

}
function macrosStatut(e){
	if(e.getAttribute("statut")!="actif"){
		e.setAttribute("cssText",e.style.cssText);
		e.setAttribute("statut","actif");
		e.style.border="solid";
		e.style.borderColor="lavender";
		var efonction=e.getAttribute("fonction");
		macros.fonctions.efonction="active";
		console.log(macros);
	}else{
		e.setAttribute("statut","inactif");
		e.style.cssText=e.getAttribute("cssText");
	}
}
var macros={
	element:[],
	fonction:[],
	brush:[],
}
function initMacros(){
	var macros=document.getElementsByClassName("macro");
	for (var i = macros.length - 1; i >= 0; i--) {
		var macro=macros[i];
		macro.onclick = function() {
			macrosStatut(this);
		}
	}
}
window.onload = function ssetup(){
	initMacros();
	initByUrl();
	statut.setup="done";
	var initsDraw=setInterval(sdraw, 25);
	console.log("statut.setup",statut.setup);
}
function sdraw(){
	if(user.statut=="reading"){
		sonreading();
	}
	if(user.statut=="writing"){
		sonwriting();
	}
}
function sonreading(){
	//updateUrl();

}
function sonwriting(){

}
function sonscroll(){

}

function sonresize(){

}
function updateUrl(){
	if(champ.location!=undefined&&champ.x!=undefined&&champ.y!=undefined&&champ.z!=undefined){
		ChangeUrl("Plenty Of Room"+String(champ.location),"#<location>"+champ.location+"</location><x>"+champ.x+"</x><y>"+champ.y+"</y><z>"+champ.z+"</z>");
	}
}
function ChangeUrl(title, url) {
	if (typeof (history.pushState) != "undefined") {
		var obj = { Title: title, Url: url };
		history.pushState(obj, obj.Title, obj.Url);
	} else {
		alert("Browser does not support HTML5.");
	}
}
function displayInputs(){
	console.log("displaying inputs");
}
function createInput(e_tag,e_id,e_class,e_cssTxt,e_html,e_x,e_y,e_z,e_w,e_h,p_x,p_y,p_z){
	var newInput=createElement(e_tag);
	newInput.id=e_id;
	newInput.className=e_class;
	newInput.style.cssText=e_cssTxt;
	newInput.style.innerHTML=e_html;
	newInput.setAttribute("e_x",e_x);
	newInput.setAttribute("e_y",e_y);
	newInput.setAttribute("e_z",e_z);
	newInput.setAttribute("e_w",e_w);
	newInput.setAttribute("e_h",e_h);
	newInput.setAttribute("e_w",e_w);
	newInput.setAttribute("p_x",e_h);
	newInput.setAttribute("p_y",e_h);
	newInput.setAttribute("p_z",e_h);

}


