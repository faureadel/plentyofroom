 var initdraw=setInterval(draw, 17);
 var timer={
 	cmd:0,
 }
var functions_onload = new Array(); //L'array qui sert au stockage des fonctions

function start_onload(fonction) { //Fonction qui devra être appelée à la place de window.onload
    functions_onload[functions_onload.length] = fonction; //On stocke les fonctions dans l'array. Il commence à 0, et length donne l'indice du dernier élément + 1.
}

window.onload = function (){
    for(var i = 0, longueur = functions_onload.length; i < longueur; i++){ //On utilise longueur pour ne pas recalculer la taille à chaque fois.
        functions_onload[i](); //On exécute les fonctions.
    }
   
}
var functions_onmousedown = new Array(); //L'array qui sert au stockage des fonctions

function start_onmousedown(fonction) { //Fonction qui devra être appelée à la place de window.onload
    functions_onmousedown[functions_onmousedown.length] = fonction; //On stocke les fonctions dans l'array. Il commence à 0, et length donne l'indice du dernier élément + 1.
}
document.onmousedown=function(event){
	for(var i = 0, longueur = functions_onmousedown.length; i < longueur; i++){ //On utilise longueur pour ne pas recalculer la taille à chaque fois.
        functions_onmousedown[i](); //On exécute les fonctions.
    }
}
var functions_onmouseup = new Array(); //L'array qui sert au stockage des fonctions

function start_onmouseup(fonction) { //Fonction qui devra être appelée à la place de window.onload
    functions_onmouseup[functions_onmouseup.length] = fonction; //On stocke les fonctions dans l'array. Il commence à 0, et length donne l'indice du dernier élément + 1.
}
document.onmouseup=function(e){
	for(var i = 0, longueur = functions_onmouseup.length; i < longueur; i++){ //On utilise longueur pour ne pas recalculer la taille à chaque fois.
        functions_onmouseup[i](); //On exécute les fonctions.
    }
}
var functions_onmousemove = new Array(); //L'array qui sert au stockage des fonctions

function start_onmousemove(fonction) { //Fonction qui devra être appelée à la place de window.onload
    functions_onmousemove[functions_onmousemove.length] = fonction; //On stocke les fonctions dans l'array. Il commence à 0, et length donne l'indice du dernier élément + 1.
}
document.onmousemove = function(e) { 
	for(var i = 0, longueur = functions_onmousemove.length; i < longueur; i++){ //On utilise longueur pour ne pas recalculer la taille à chaque fois.
        functions_onmousemove[i](); //On exécute les fonctions.
    }

}
var functions_onkeydown = new Array(); //L'array qui sert au stockage des fonctions

function start_onkeydown(fonction) { //Fonction qui devra être appelée à la place de window.onload
    functions_onkeydown[functions_onkeydown.length] = fonction; //On stocke les fonctions dans l'array. Il commence à 0, et length donne l'indice du dernier élément + 1.
}
document.onkeydown = function(e){
		for(var i = 0, longueur = functions_onkeydown.length; i < longueur; i++){ //On utilise longueur pour ne pas recalculer la taille à chaque fois.
        functions_onkeydown[i](); //On exécute les fonctions.
    }
}
var functions_onkeyup = new Array(); //L'array qui sert au stockage des fonctions

function start_onkeyup(fonction) { //Fonction qui devra être appelée à la place de window.onload
    functions_onkeyup[functions_onkeyup.length] = fonction; //On stocke les fonctions dans l'array. Il commence à 0, et length donne l'indice du dernier élément + 1.
}
document.onkeyup = function(e){
		for(var i = 0, longueur = functions_onkeyup.length; i < longueur; i++){ //On utilise longueur pour ne pas recalculer la taille à chaque fois.
        functions_onkeyup[i](); //On exécute les fonctions.
    }
}

var functions_ondraw = new Array(); //L'array qui sert au stockage des fonctions

function start_ondraw(fonction) { //Fonction qui devra être appelée à la place de window.onload
    functions_ondraw[functions_ondraw.length] = fonction; //On stocke les fonctions dans l'array. Il commence à 0, et length donne l'indice du dernier élément + 1.
}
function draw(){
		for(var i = 0, longueur = functions_ondraw.length; i < longueur; i++){ //On utilise longueur pour ne pas recalculer la taille à chaque fois.
        functions_ondraw[i](); //On exécute les fonctions.
    }
}
function random(min, max) {
	return Math.random() * (max - min) + min;
}

function rect(elem){
	var bodyRect = document.body.getBoundingClientRect(),
	elemRect = elem.getBoundingClientRect();
	elem.setAttribute("w",elemRect.width);
	elem.setAttribute("h",elemRect.height);
	offset  = elemRect.top - bodyRect.top;
	elem.setAttribute("y",offset);
	offset  = elemRect.left - bodyRect.left;
	elem.setAttribute("x",offset);
	return{
		x: parseFloat(elem.getAttribute("x")),
		y: parseFloat(elem.getAttribute("y")),
		w: parseFloat(elem.getAttribute("w")),
		h: parseFloat(elem.getAttribute("h"))
	}
}

function rectAttributes(elem){
	try{
		var bodyRect = document.body.getBoundingClientRect(),
		elemRect = elem.getBoundingClientRect();
		elem.setAttribute("w",elemRect.width);
		elem.setAttribute("h",elemRect.height);
		offset  = elemRect.top - bodyRect.top;
		elem.setAttribute("y",offset);
		offset  = elemRect.left - bodyRect.left;
		elem.setAttribute("x",offset);
	}catch(e){
		console.log("function rectAttributes FAIL",e);
	}
}
function print(name,what){
		if(what==undefined){
			what='';
		}else{
			what=" = "+what;
		}
		var input = document.createElement('p');
		input.appendChild(document.createTextNode(timer.cmd+" "+name+what));
		var where=document.getElementById("console");
		where.insertBefore(input, where.firstChild);
		timer.cmd++;
	}
