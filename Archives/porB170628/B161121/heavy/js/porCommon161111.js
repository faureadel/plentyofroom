var initcommonDraw=setInterval(commondraw, 25);
var user={
	name:undefined,
	statut:undefined,
	mouse:{
		moving:false,
		historyx:[],
		historyy:[],
		x:undefined,
		y:undefined,
		dragging:false,
		drag:{
			path:{
				x:[],
				y:[],
			}
		},
		pressing:false,
		down:{
			x:undefined,
			y:undefined,
			historyx:[],
			historyy:[],
		},
		up:{
			x:undefined,
			y:undefined,
			historyx:[],
			historyy:[],
		},
	},
	keys:{
		pressing:false,
		down:[],
		downhistory:[],
		up:undefined,
		uphistory:[],
	},
	window:{
		resizing:true,
		scrolling:true,
		html:document.getElementsByTagName("body")[0],
		x:undefined,
		y:undefined,
		w:undefined,
		h:undefined,
		scrolly:0,
	}
};
var commondraw={
	mousehistorylength:0,
	statutTimer:{
		reading:0,
		idle:0,
		writting:0,
		busy:0,
	}
}
document.onmousedown=function(e){
	if(user.mouse.pressing==false){
		user.mouse.pressing=true;
		user.mouse.down.x=event.clientX;
		user.mouse.down.historyx.push(event.clientX);
		user.mouse.down.historyy.push(event.clientY);
		user.mouse.down.y=event.clientY;
		//console.log("user.mouse.pressing",user.mouse.pressing);
		//console.log("user.mouse.down.x",user.mouse.down.x);
		//console.log("user.mouse.down.y",user.mouse.down.y);
		//console.log("user.mouse.down.historyx",user.mouse.down.historyx);
		//console.log("user.mouse.down.historyy",user.mouse.down.historyy);
	}
}
document.onmouseup=function(e){
	if(user.mouse.pressing==true){
		user.mouse.pressing=false;
		user.mouse.up.x=event.clientX;
		user.mouse.up.historyx.push(event.clientX);
		user.mouse.up.historyy.push(event.clientY);
		user.mouse.up.y=event.clientY;
		//console.log("user.mouse.pressing",user.mouse.pressing);
		//console.log("user.mouse.up.x",user.mouse.up.x);
		//console.log("user.mouse.up.y",user.mouse.up.y);
		//console.log("user.mouse.up.historyx",user.mouse.up.historyx);
		//console.log("user.mouse.up.historyy",user.mouse.up.historyy);
	}
}
document.onmousemove = function(e) { 
	if(user.mouse.moving==false){
		user.mouse.moving=true;
		//console.log("user.mouse.moving",user.mouse.moving);
	}
	user.mouse.historyx.push(event.clientX);
	user.mouse.historyy.push(event.clientY);
	user.mouse.x=event.clientX;
	user.mouse.y=event.clientY;
	//console.log("user.mouse.x",user.mouse.x);
	//console.log("user.mouse.y",user.mouse.y);
	//console.log("user.mouse.historyx",user.mouse.historyx);
	//console.log("user.mouse.historyy",user.mouse.historyy);
}
function onmousedrag(e){
	e.drag.path.x.push(e.x);
	e.drag.path.y.push(e.y);

}
document.onkeydown = function(e){
	e = e || window.event;
	var key = e.which || e.keyCode;
	var keysdown="";
	for (var i = user.keys.down.length - 1; i >= 0; i--) {
		keysdown= keysdown+String(user.keys.down[i])+',';
	}
	if(keysdown.includes(key+',')==false){
		user.keys.down.push(key);
		//console.log("keysdown",keysdown);
		//console.log("user.keys.down",user.keys.down);
		user.keys.downhistory.push(key);
	}
}
document.onkeyup = function(e){
	e = e || window.event;
	var key = e.which || e.keyCode;
	var keysdown="";
	var newKeysDown=[];
	for (var i = user.keys.down.length - 1; i >= 0; i--) {
		if(key!=user.keys.down[i]){
			newKeysDown.push(user.keys.down[i]);
		}else{
			user.keys.up=key;
			//console.log("user.keys.up",user.keys.up);
			user.keys.uphistory.push(key);
		}
	}
	user.keys.down=newKeysDown;
	//console.log("user.keys.down",user.keys.down);

}
function rectAttributes(elem){
	try{
		var bodyRect = document.body.getBoundingClientRect(),
		elemRect = elem.getBoundingClientRect();
		elem.setAttribute("w",elemRect.width);
		elem.setAttribute("h",elemRect.height);
		offset  = elemRect.top - bodyRect.top;
		elem.setAttribute("y",offset);
		offset  = elemRect.left - bodyRect.left;
		elem.setAttribute("x",offset);
	}catch(e){
		console.log("function rectAttributes FAIL",e);
	}
}
function commondraw(){
	if(window.scrollY!=user.window.scrolly){
		if(user.window.scrolling==false){
			user.window.scrolling=true;
			//console.log("user.window.scrolling",user.window.scrolling);
		}
	}else{
		if(user.window.scrolling==true){
			user.window.scrolling=false;
			//console.log("user.window.scrolling",user.window.scrolling);
		}
	}
	user.window.scrolly=window.scrollY;

	try{
		rectAttributes(user.window.html);
		if(parseFloat(user.window.html.getAttribute("w"))!=parseFloat(user.window.w) || parseFloat(user.window.html.getAttribute("h"))!=parseFloat(user.window.h)){

			if(user.window.resizing!=true){
				user.window.resizing=true;
				//console.log("user.window.resizing",user.window.resizing);
			}
		}
		if(parseFloat(user.window.html.getAttribute("w"))==parseFloat(user.window.w) && parseFloat(user.window.html.getAttribute("h"))==parseFloat(user.window.h)){
			if(user.window.resizing!=false){
				user.window.resizing=false;
				//console.log("user.window.resizing",user.window.resizing);
			}
		}
	}catch(e){
		console.log("error",e);
		user.window.html=document.getElementsByTagName("body")[0];
		console.log("user.window.html",user.window.html);
		rectAttributes(user.window.html);
		user.window.w=user.window.html.getAttribute("w");
		user.window.h=user.window.html.getAttribute("h");
	}
	if(user.window.w!=user.window.html.getAttribute("w")){
		user.window.w=user.window.html.getAttribute("w");
	}
	if(user.window.h!=user.window.html.getAttribute("h")){
		user.window.h=user.window.html.getAttribute("h");
	}
	if(user.mouse.pressing==false && user.mouse.moving==false && user.keys.pressing==false){

		if(commondraw.statutTimer.reading<125){
			if(user.statut!="reading"){
				user.statut="reading";
				//console.log("user.statut",user.statut);
			}
			commondraw.statutTimer.reading++;
		}else{
			if(user.statut!="idle"){
				user.statut="idle";
				//console.log("user.statut",user.statut);
			}
		}
		if(commondraw.statutTimer.writting!=0){
			commondraw.statutTimer.writting=0;
		}
	}else{
		if(commondraw.statutTimer.writting<125){
			if(user.statut!="writting"){
				user.statut="writting";
				//console.log("user.statut",user.statut);
			}
			commondraw.statutTimer.writting++;
		}else{
			if(user.statut!="busy"){
				user.statut="busy";
				//console.log("user.statut",user.statut);
			}
		}
		if(commondraw.statutTimer.reading!=0){
			commondraw.statutTimer.reading=0;
		}
	}


	if(user.keys.down.length==0){
		if(user.keys.pressing==true){
			user.keys.pressing=false;
			//console.log("user.keys.pressing",user.keys.pressing);
		}
	}else{
		if(user.keys.pressing==false){
			user.keys.pressing=true;
			//console.log("user.keys.pressing",user.keys.pressing);
		}
	}
	if(user.mouse.moving==true){
		if(user.mouse.historyx.length==commondraw.mousehistorylength){
			user.mouse.moving=false;
			user.mouse.historyx=[];
			user.mouse.historyy=[];
			//console.log("user.mouse.moving",user.mouse.moving);
		}else{
			commondraw.mousehistorylength=user.mouse.historyx.length;
		}
		if(user.mouse.pressing==true){
			onmousedrag(user.mouse);
			if(user.mouse.dragging==false){
				user.mouse.dragging=true;
				//console.log("user.mouse.dragging",user.mouse.dragging);
			}
		}
	}else{
		if(user.mouse.dragging==true){
			user.mouse.dragging=false;
			//console.log("user.mouse.dragging",user.mouse.dragging);
			
		}
		if(user.mouse.dragging==false){
			user.mouse.drag.path.x=[];
			user.mouse.drag.path.y=[];
		}
	}
	
}
function random(min, max) {
	return Math.random() * (max - min) + min;
}