(function(global){

    //namespace
    /**
    * Board constructor, a simple div using CSS3D in which BoardElements can be appended
    * @class Board
    * @param {Number} width
    * @param {Number} height
    */
    Board = function(width, height){

        /**
        The underlying DOM element managed by the Board object.
        @property DOMElement {DOM Object}
        */
        this.DOMElement = document.createElement("div");
        this.DOMElement.style.position = "absolute";

        //TODO : faire un mode auto et un mode fixe pour ça!!
        //this.DOMElement.style.overflow = "hidden";

        this.DOMElement.style.width = width || "100%";
        this.DOMElement.style.height = height || "100%";

        document.body.appendChild(this.DOMElement);

        var perspective = "1500px";
        this.DOMElement.style.webkitPerspective = perspective;
        this.DOMElement.style.MozPerspective = perspective;
        this.DOMElement.style.msPerspective = perspective;
        this.DOMElement.style.OPerspective = perspective;
        this.DOMElement.style.perspective = perspective;

        /**
        The list of all BoardElement attached to this board
        @property elements {Array}
        */
        this.elements = [];

        /**
        This Board's width
        @property width {Number}
        */
        this.width = 0;
        /**
        This Board's height
        @property height {Number}
        */
        this.height = 0;

        /**
        This Board's homeButton, a fixed positionned text BoardElement with button like events attached. This element is added to the document, not to the Board!
        @property homeButton {BoardElement}
        */
        this.homeButton = new BoardElement("home-button");
        this.homeButton.createImage("lib/home.png",Board.homeLoaded);
        this.homeButton.setPositionFixed(true);

        this.homeButton.setOnClickEvent(Board.goToHomeEvent.bind(this.homeButton));
        this.homeButton.setOnOverEvent(Board.overHomeEvent.bind(this.homeButton));
        this.homeButton.setOnOutEvent(Board.outHomeEvent.bind(this.homeButton));
        this.homeButton.url = "../../index.html";
        document.body.appendChild(this.homeButton.DOMElement);

        window.addEventListener("orientationchange", Board.onWindowResize.bind(this.homeButton));
        window.addEventListener("resize", Board.onWindowResize.bind(this.homeButton));

    };

    Board.homeLoaded = function(){
        console.log("homeLoaded",this);
        this.setAspectWidth(50);
        this.setPositionY(window.innerHeight - this.height);
    };

    Board.goToHomeEvent = function(){
        console.log("click",this);
        window.location = this.url;
    };
    Board.overHomeEvent = function(){
        this.setImage("lib/home-over.png");
        //console.log("mouseover", this);
    };
    Board.outHomeEvent = function(){
        this.setImage("lib/home.png");
        //console.log("mouseout", this);
    };

    Board.onWindowResize = function(event){
        this.setPositionY(window.innerHeight - this.height);
    };

    /**
    * setWidth for this board. Can be expressed in simple number (converted to pixels internally) or a JS "CSS string"
    * @method setWidth
    * @param {Number || CSS string} val
    */
    Board.prototype.setWidth = function(val){

        if(typeof val === "number"){
            this.width = val;
            this.DOMElement.style.width = val+"px";
        }else if(typeof val === "string"){
            this.width = (val.replace("px", "")).valueOf();
            this.DOMElement.style.width = val;
        }
    }

    /**
    * setHeight for this board. Can be expressed in simple number (converted to pixels internally) or a JS "CSS string"
    * @method setHeight
    * @param {Number || CSS string} val
    */
    Board.prototype.setHeight = function(val){
        if(typeof val === "number"){
            this.height = val;
            this.DOMElement.style.height = val+"px";
        }else if(typeof val === "string"){
            this.height = (val.replace("px", "")).valueOf();
            this.DOMElement.style.height = val;
        }
    }

    /**
    * Define the backgroundColor ot this board
    * @method setBackgroundColor
    * @param {Number || CSS string} color number for red OR number to repeat on the 3 color channels OR CSS string
    * @param {Number} color number for green (optional)
    * @param {Number} color number for blue (optional)
    */
    Board.prototype.setBackgroundColor = function(arg1,arg2,arg3){

        if(arguments.length === 1){
            if(typeof arg1 === "string"){
                this.DOMElement.style.backgroundColor = arg1;
            }else if(typeof arg1 === "number"){
                this.DOMElement.style.backgroundColor =
                    "rgb("+arg1+","+arg1+","+arg1+")";
            }
        }else if(arguments.length === 3){
            this.DOMElement.style.backgroundColor =
                "rgb("+arg1+","+arg2+","+arg3+")";
        }
    };

    /**
    * Add a BoardElement to this board. This MUST be used if you want to see your BoardElement in the board
    * @method addElement
    * @param {BoardElement} element
    */
    Board.prototype.addElement = function(el){
        this.elements.push(el);
        this.DOMElement.appendChild(el.DOMElement);
        /*if(el.isVideo || el.isImage || el.isAudio){
            this.DOMElement.appendChild(el.caption.DOMElement);
        }*/
    };

    /**
    * Returns the BoardElement of this name
    * @method getElementByName
    * @param {String} name
    * @return {BoardElement} element which has this name
    */
    Board.prototype.getElementByName = function(name){

        for(var i=0; i<this.elements.length; i++){
            if(this.elements[i].name === name){
                return this.elements[i];
            }
        }

    };

    /**
    * BoardElement is an element that can be attached to a Board. After its creation, it can be used to create a Text, Image, Video or Audio element that will held a media and manage its transform in the Board space.
    * @class BoardElement
    *
    */
    BoardElement = function(name){

        this.DOMElement = null;

        this.name = name;

        this.text = null;
        this.image = null;
        this.video = null;
        this.audio = null;
        this.css = null;

        /**
        * The caption that will appears automatically at the bottom of this BoardElement. A caption is a just text BoardElement appended to a container div that also contains the media element.
        * @property caption
        */
        this.caption = null;//recursive element -> will be a boardElement

        this.x = 0;
        this.y = 0;
        this.z = 0;

        this.rotationX = 0;
        this.rotationY = 0;
        this.rotationZ = 0;

        this.scaleX = 1;
        this.scaleY = 1;

        this.width = 0;
        this.height = 0;
        this.aspect = 0;

        this.opacity = 1;
        this.visible = 1;

        this.isText = false;
        this.isImage = false;
        this.isVideo = false;
        this.isAudio = false;

        //this.isButton = false;
    };

    /**
    * Set the position of this element in the Board 3 axis can be used as the Board is a 3D space
    * @method setPosition
    * @param {Number} x
    * @param {Number} y
    * @param {Number} z (optional)
    * @optional
    */
    BoardElement.prototype.setPosition = function(x,y,z){
        this.x = x;
        this.y = y;
        this.z = (z !== undefined) ? z : 0;
        this.updateCSS();
    };

    //====translation====

    /**
    * Set the x position of this element in the Board
    * @method setPositionX
    * @param {Number} x
    */
    BoardElement.prototype.setPositionX = function(val){
        this.x = val;
        this.updateCSS();
    };
    /**
    * Set the y position of this element in the Board
    * @method setPositionY
    * @param {Number} y
    */
    BoardElement.prototype.setPositionY = function(val){
        this.y = val;
        this.updateCSS();
    };

    /**
    * Set the z position of this element in the Board
    * @method setPositionZ
    * @param {Number} val
    */
    BoardElement.prototype.setPositionZ = function(val){
        this.z = val;
        this.updateCSS();
    };

    /**
    * Set this BoardElement to have a "fixed" CSS position, that is fixed inside the current window in pixel unit.
    * @method setPositionFixed
    * @param {Boolean} bool
    */
    BoardElement.prototype.setPositionFixed = function(bool){
        this.DOMElement.style.position = bool ? "fixed" : "absolute";
        //console.info("setPositionFixed will cancel every setPosition calls")
    };

    //===rotation====

    /**
    * Set the z rotation of this element in the Board
    * @method setRotation
    * @param {Number} z
    */
    BoardElement.prototype.setRotation = function(z){
        this.rotationZ = z;
        this.updateCSS();
    };

    /**
    * Set the x rotation of this element in the Board
    * @method setRotationX
    * @param {Number} x
    */
    BoardElement.prototype.setRotationX = function(val){
        this.rotationX = val;
        this.updateCSS();
    };
    /**
    * Set the y rotation of this element in the Board
    * @method setRotationY
    * @param {Number} y
    */
    BoardElement.prototype.setRotationY = function(val){
        this.rotationY = val;
        this.updateCSS();
    };

    /**
    * Set the z rotation of this element in the Board == setRotation
    * @method setRotationZ
    * @param {Number} val
    */
    BoardElement.prototype.setRotationZ = function(val){
        this.setRotation(val);
    };

    //===scale====

    /**
    * Set the x and y scale of this element in the Board (z scale is not applicable to flat objects!)
    * @method setScale
    * @param {Number} x
    * @param {Number} y
    */
    BoardElement.prototype.setScale = function(x,y){
        this.scaleX = x;
        this.scaleY = y;
        this.updateCSS();
    };

    /**
    * Set the x scale of this element in the Board (z scale is not applicable to flat objects!)
    * @method setScaleX
    * @param {Number} x
    */
    BoardElement.prototype.setScaleX = function(val){
        this.scaleX = val;
        this.updateCSS();
    };

    /**
    * Set the y scale of this element in the Board (z scale is not applicable to flat objects!)
    * @method setScaleY
    * @param {Number} y
    */
    BoardElement.prototype.setScaleY = function(val){
        this.scaleY = val;
        this.updateCSS();
    };

    //=====width height====

    /**
    * Set this BoardElement's width (not it's scale, which is a transform, but its width in pixels)
    * @method setWidth
    * @param {Number} val
    */
    BoardElement.prototype.setWidth = function(val){
        this.width = val;
        this.updateCSS();
    };

    /**
    * Set this BoardElement's height (not it's scale, which is a transform, but its width in pixels)
    * @method setHeight
    * @param {Number} val
    */
    BoardElement.prototype.setHeight = function(val){
        this.height = val;
        this.updateCSS();
    }

    /**
    * Set this BoardElement's width and calculate its height according to its orginal aspect ratio
    * @method setAspectWidth
    * @param {Number} val
    */
    BoardElement.prototype.setAspectWidth = function(val){
        if(this.aspect !== 0){
            this.width = val;
            this.height = this.width/this.aspect;
            this.updateCSS();
        }
    };

    //======CSS=======

    /**
    * Set this BoardElement's CSS class. Anything that the CSS class is defining will be setted to the underlying DOMElement of this BoardElement. Particulary handy for text (font, text, color), margins, etc. NOTE : in your css definitions, class are declared with a '.', like this '.myClass'; be sure not to use erase this '.' in the argument you pass to this method, like this : element.setCSSClass("myClass");
    * @method setCSSClass
    * @param {String} cssName
    */
    BoardElement.prototype.setCSSClass = function(cssName){

        var cssRules = document.styleSheets[0].cssRules;
        if(cssRules){
            for(var i=0; i<cssRules.length; i++){

                var selector = cssRules[i].selectorText;

                if(selector){
                    if(selector.indexOf(cssName) > 0){
                        //apply css
                        this.DOMElement.classList.add(cssName);
                        return;
                        //break;
                    }
                }else{
                    //console.info("no selectorText in this css",cssRules[i]);
                }
            }
        }
        console.error("no css class of this name found. Remove the . if any in your argument")
    };

    //=====BUTTON=====

    /**
    * Defines the callback to use for the click event of this BoardElement
    * @method setOnClickEvent
    * @param {Object} callback
    */
    BoardElement.prototype.setOnClickEvent = function(callback){
        this.DOMElement.addEventListener("click",callback);
    };
    /**
    * Defines the callback to use for the mouseover event of this BoardElement
    * @method setOnOverEvent
    * @param {Object} callback
    */
    BoardElement.prototype.setOnOverEvent = function(callback){
        this.DOMElement.addEventListener("mouseover", callback);
    };
    /**
    * Defines the callback to use for the mouseout event of this BoardElement
    * @method setOnOutEvent
    * @param {Object} callback
    */
    BoardElement.prototype.setOnOutEvent = function(callback){
        this.DOMElement.addEventListener("mouseout", callback);
    };


    //=============
    //Special objects creators
    //=============

    //=====TEXT=====

    /**
    * Create a text BoardElement after its construction.
    * @method createText
    * @param {String} string
    */
    BoardElement.prototype.createText = function(string){

        this.DOMElement = document.createElement("div");
        this.text = string || null;
        this.isText = true;

        this.setCommonStyle();

    };

    /**
    * Changes the text of this Element, if it's a text element only.
    * @method setText
    * @param {String} string
    */
    BoardElement.prototype.setText = function(string){
        if(this.isText){
            this.text = string;
            this.setCommonStyle();
        }
    };

    /**
    * Changes the background color of this element (usefull for text)
    * @method setBackgroundColor
    * @param {CSSString} color the color css string (i.e "white" or "rgb(255,255,255)")
    */
    BoardElement.prototype.setBackgroundColor = function(color){
        if(this.isText){
            this.DOMElement.style.backgroundColor(color);
        }
    };


    //=====IMAGE=====

    /**
    * Creates an Image BoardElement once this BoardElement has been constructed. Picture files supported formats are the sandard ones of HTML5.
    * @method createImage
    * @param {String} url where to get the file from
    * @param {Object} callback the callback that will be executed when the media file will be loaded (this is just a function you declare in your script)
    */
    BoardElement.prototype.createImage = function(url, callback){

        this.DOMElement = document.createElement("div");

        this.isImage = true;
        this.image = url;

        this.DOMMedia = document.createElement("img");
        this.DOMMedia.addEventListener("load", BoardElement.imgLoadHandler.bind(this));
        this.imageLoadCallback = callback;

        this.caption = new BoardElement();
        this.caption.createText();

        this.setCommonStyle();

        this.DOMElement.appendChild(this.DOMMedia);
        this.DOMElement.appendChild(this.caption.DOMElement);
    };

    /**
    * Description for setImage
    * @method setImage
    * @param {Object} url
    */
    BoardElement.prototype.setImage = function(url){
        this.image = url;
        this.DOMMedia.src = url;
        this.updateCSS();
    };

    BoardElement.imgLoadHandler = function(event){

        console.log("img loaded",event, this);
        var el = event.srcElement;

        this.width = el.naturalWidth;
        this.height = el.naturalHeight;

        this.aspect = this.width/this.height;

        el.style.width =  "100%";
        el.style.height =  "100%";

        this.DOMElement.style.width = this.width + "px";
        this.DOMElement.style.height =  this.height + "px";

        if(this.imageLoadCallback !== undefined){
            this.imageLoadCallback(this);//pass the current boardElement
        }
        //update caption post callback as we usually set position there
        this.caption.setWidth(this.width);
        // this.caption.setPosition(this.x, this.y+this.height);
    }

    //========VIDEO=====

    /**
    * Creates a Video BoardElement once this BoardElement has been constructed. A video element will appear on screen as a media player containing a play/pause button, a playhead and timeline and a volume slider. Video files supported formats are the sandard ones of HTML5.
    * @method createVideo
    * @param {String} url where to get the mp4 or ogg file from
    * @param {Object} callback the callback that will be executed when the media file will be loaded (this is just a function you declare in your script)
    */
    BoardElement.prototype.createVideo = function(url, callback){

        this.DOMElement = document.createElement("div");

        this.video = url;
        this.isVideo = true;

        this.DOMMedia = document.createElement("video");
        this.DOMMedia.addEventListener("canplay", BoardElement.videoLoadHandler.bind(this));
        this.DOMMedia.controls = true;

        this.videoLoadCallback = callback;

        this.caption = new BoardElement();
        this.caption.createText();

        this.DOMElement.appendChild(this.DOMMedia);
        this.DOMElement.appendChild(this.caption.DOMElement);

        this.setCommonStyle();
    }

    BoardElement.videoLoadHandler = function(event){
        //NB : this is the current BoardElement
        console.log("video loaded",event, this);

        var el = event.srcElement;//get the DOM element
        this.width = el.videoWidth;
        this.height = el.videoHeight;

        this.aspect = this.width/this.height;

        el.style.width = "100%";
        el.style.height =  "100%";

        this.DOMElement.style.width =  this.width+"px";
        this.DOMElement.style.height =  this.height+"px";

        if(this.videoLoadCallback !== undefined){
            this.videoLoadCallback(this);
        }
        //update caption post callback as we usually set position there
        this.caption.setWidth(this.width);
        //this.caption.setPosition(this.x, this.y+this.height);
    }

    /**
    * Defines the poster picture file for this video element, that is the picture that is displayed before the video is played or loaded.
    * @method setPoster
    * @param {String} url
    */
    BoardElement.prototype.setPoster = function(url){
        if(this.isVideo){
            this.DOMMedia.poster = url;
        }
    }
    //=======AUDIO====

    /**
    * Creates an Audio BoardElement once this BoardElement has been constructed. An audio element will appear on screen as a media player containing a play/pause button, a playhead and timeline and a volume slider. Audio files supported formats are the sandard ones of HTML5.
    * @method createAudio
    * @param {String} url where to get the mp3 or wav file from
    * @param {Object} callback the callback that will be executed when the media file will be loaded (this is just a function you declare in your script)
    */
    BoardElement.prototype.createAudio = function(url, callback){

        this.DOMElement = document.createElement("div");

        this.audio = url;
        this.isAudio = true;

        this.DOMMedia = document.createElement("audio");
        this.DOMMedia.addEventListener("canplay", BoardElement.audioLoadHandler.bind(this));
        this.DOMMedia.controls = true;
        this.audioLoadCallback = callback;

        this.caption = new BoardElement();
        this.caption.createText();

        this.DOMElement.appendChild(this.DOMMedia);
        this.DOMElement.appendChild(this.caption.DOMElement);

        this.setCommonStyle();
    }

    BoardElement.audioLoadHandler = function(event){

        console.log("audio loaded",event, this);

        var el = event.srcElement;

        this.width = 300;
        this.height = 50;

        el.style.width =  "100%";
        el.style.height =  "100%";

        this.DOMElement.style.width =  this.width+"px";
        this.DOMElement.style.height =  this.height+"px";

        if(this.audioLoadCallback !== undefined){
            this.audioLoadCallback(this);
        }
        //update caption post callback as we usually set position there
        this.caption.setWidth(this.width);
        //this.caption.setPosition(this.x, this.y+this.height);
    }

    /**
    * Gives some common css value to all element at construction. Used internally only.
    * @private
    * @method setCommonStyle
    */
    BoardElement.prototype.setCommonStyle = function(){

        var el = this.DOMElement;
        el.style.position = "absolute";
        el.style.width =  this.width + "px";
        el.style.height =  this.height + "px";

        if(this.isText){
            el.innerHTML = this.text;
        }else if(this.isImage){
            el = this.DOMMedia;
            el.src = this.image;
            //next is asynchrous, so need to use eventHandler
        }else if(this.isVideo){
            el = this.DOMMedia;
            el.src = this.video;
            //next is asynchrous, so need to use eventHandler
        }else if(this.isAudio){
            el = this.DOMMedia;
            el.src = this.audio;
            //next is asynchrous, so need to use eventHandler
        }
    };

    /**
    * updates this BoardElement underlying DOMElement css. Used internally only.
    * @private
    * @method updateCSS
    */
    BoardElement.prototype.updateCSS = function(){

        var el = this.DOMElement;

        el.style.width =  this.width + "px";
        el.style.height =  this.height + "px";
        /*el.style.left =  this.x + "px";
        el.style.top =  this.y + "px";*/

        var CSSString = "translate3d("+this.x+"px,"+this.y+"px,"+this.z+"px) ";
        CSSString += "rotateX("+this.rotationX+"deg) rotateY("+this.rotationY+"deg) rotateZ("+this.rotationZ+"deg) ";
        CSSString += "scale3d("+this.scaleX+","+this.scaleY+",1)";

        //console.log(CSSString);
        el.style.webkitTransform = CSSString;
        el.style.MozTransform = CSSString;
        el.style.msTransform = CSSString;
        el.style.OTransform = CSSString;
        el.style.transform = CSSString;
    }

    //========
    //board utils
    //========

    /**
    * BoardUtils contains some utilitaries method to do some stuff in an easy fashion
    * @class BoardUtils
    *
    */
    BoardUtils = {};

    /**
    * Computes a random number beteween min and max
    * @method randomFromTo
    * @return a random float number {Number}
    */
    BoardUtils.randomFromTo = function(min,max){

        var randomIndex = Math.random() * (max - min) + min ;
        return randomIndex;
    }

})(this);
