var pointeur;
var oldCamera={
	x:0,
	y:0,
	z:1,
};
var camera={
	x:0,
	y:0,
	z:1,
	centre_x:undefined,
	centre_y:undefined,
	vitesse:1,
	left:false,
	right:false,
	up:false,
	down:false,
	in:false,
	out:false,
	in_use:false,

};
ghost = function(){
	this.x;
	this.y;
	this.w;
	this.h;
	this.type;
	this.DOMElement;
}
var ghosts=[];
function createForm(el){
	/*DOM*/
	var child=document.createElement('div');
	child.id=ghosts.length;
	child.className='child';
	child.setAttribute('type','form');
	child.style.width=rect(el).w*2+'px';
	child.style.height=rect(el).h*2+'px';
	child.style.marginLeft=rect(el).x-rect(el).w/2+'px';
	child.style.marginTop=rect(el).y-rect(el).w/2+'px';
	var child_0=document.createElement('div');
	child_0.style.width=rect(el).w*2+'px';
	child_0.style.height=rect(el).h*2+'px';
	child_0.style.backgroundImage = "url('data/assets/form"+random(0,4)+".gif')";
	child_0.style.backgroundSize = '100%';
	child_0.style.backgroundRepeat = 'none';
	child_0.style.transform = 'rotate('+random(-360,360)+'deg)';
	child.appendChild(child_0);
	document.body.appendChild(child);
	el.style.backgroundColor='transparent';
	/*POR*/
	var g = new ghost();
	g.DOMElement=child;
	g.x=camera.centre_x-(camera.centre_x-(rect(el).x-rect(el).w/2-camera.x*camera.z))/camera.z;
	g.y=camera.centre_y-(camera.centre_y-(rect(el).y-rect(el).h/2-camera.y*camera.z))/camera.z;
	g.w=rect(el).w*2/camera.z;
	g.h=rect(el).h*2/camera.z;
	g.type='form';
	ghosts.push(g);
}
function camera_moves(e,bool){
	e = e || window.event;
	var key = e.which || e.keyCode;
	console.log(key);
	if(key==39){
		camera.right=bool;
	}
	if(key==37){
		camera.left=bool;
	}
	if(key==38){
		camera.up=bool;
	}
	if(key==40){
		camera.down=bool;
	}
	if(key==33){
		camera.in=bool;
	}
	if(key==34){
		camera.out=bool;
	}
}