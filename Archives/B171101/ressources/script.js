var cameras = [];

var Camera = function(options){
	this.x = check(options,'x',0);
	this.y = check(options,'y',0);
	this.z = check(options,'z',1);
	this.centreX = check(options,'centreX',window.innerWidth/2);
	this.centreY = check(options,'centreY',window.innerHeight/2);
	this.left = false;
	this.up = false;
	this.right = false;
	this.down = false;
	this.in = false;
	this.out = false;
	this.vitesse = 8;
	cameras.push(this);
}
Camera.prototype.move = function(){
	if(this.up==true){
		this.y=this.y-(1*this.vitesse)/this.z;
	}
	if(this.right==true){
		this.x=this.x+(1*this.vitesse)/this.z;
	}
	if(this.left==true){
		this.x=this.x-(1*this.vitesse)/this.z;
	}
	if(this.down==true){
		this.y=this.y+(1*this.vitesse)/this.z;
	}
	if(this.in==true){
		this.z=this.z*(1+this.vitesse/350);
	}
	if(this.out==true){
		this.z=this.z/(1+this.vitesse/350);
	}
}

var elements={
	all:[],
	interface:[]
};

var Element = function(options){
	this.name = check(options,'name','el'+elements.all.length);
	this.camera = check(options,'camera',cameras[0]);
	//this.screenX = check(options,'x',0);
	//this.screenY = check(options,'y',0);
	this.x = parseInt((this.camera.centreX-(this.camera.centreX-check(options,'x',0)+this.camera.x*this.camera.z)/this.camera.z)/baseGrid)*baseGrid;
	this.y = parseInt((this.camera.centreY-(this.camera.centreY-check(options,'y',0)+this.camera.y*this.camera.z)/this.camera.z)/baseGrid)*baseGrid;
	//this.screenW = check(options,'w',0);
	//this.screenH = check(options,'h',0);
	this.w = check(options,'w',0)/this.camera.z;
	this.h = check(options,'h',0)/this.camera.z;
	this.z = check(options,'z',1);
	this.rect = check(options,'rect',{});
	this.parent = check(options,'parent',document.body.getElementsByTagName('main')[0]);
	this.dom = document.createElement(check(options,'dom','div'));
	this.dom.style.zIndex=elements.length;
	this.dom.id=this.name;
	this.dom.className='el';
	this.dom.element=this;
	elements.all.push(this);
}

Element.prototype.update = function(options){
	if(document.getElementById(this.name)==null){
		this.parent.appendChild(this.dom);
	}
	this.rect={
		view:10*(this.z-this.camera.z)/this.camera.z,
		x:this.camera.centreX-(this.camera.centreX-this.x-this.camera.x)*this.camera.z,
		y:this.camera.centreY-(this.camera.centreY-this.y-this.camera.y)*this.camera.z,
		w:this.w*this.camera.z,
		h:this.h*this.camera.z,
		opacity:(this.w*this.camera.z-this.w*this.z)/2500
	}

	if(this.rect.x + this.rect.w > this.rect.view && this.rect.y + this.rect.h > this.rect.view && this.rect.x < window.innerWidth-this.rect.view && this.rect.y < window.innerHeight-this.rect.view && this.rect.opacity<1){
		updateCSS(this.dom,{
			marginLeft:this.rect.x+'px',
			marginTop:this.rect.y+'px',
			width:this.rect.w+'px',
			height:this.rect.h+'px',
			opacity:1-this.rect.opacity,
			fontSize:rect(this.dom).w/parseInt(rect(this.dom).h/rect(this.dom).w)+'px',
		});
	}else{
		this.parent.removeChild(this.dom);
	}
}

function preview (options){
	return{
		x:parseInt((cameras[0].centreX-(cameras[0].centreX-check(options,'x',0)+cameras[0].x*cameras[0].z)/cameras[0].z)/baseGrid)*baseGrid,
		y:parseInt((cameras[0].centreY-(cameras[0].centreY-check(options,'y',0)+cameras[0].y*cameras[0].z)/cameras[0].z)/baseGrid)*baseGrid,
		w:check(options,'w',0)/cameras[0].z,
		h:check(options,'h',0)/cameras[0].z,
	}
	
}
var textArea = document.createElement('textArea');
document.onkeydown = function(e){
	
	if(document.body.selection!=undefined){
		console.log(document.body.selection);
		textArea.focus();
		document.body.selection.textContent=textArea.value;
	}else{
		document.body.focus();
		
	}
	//console.log(e.keyCode);
	if(e.keyCode==37){
		cameras[0].left=true;
	}
	if(e.keyCode==38){
		cameras[0].up=true;
	}
	if(e.keyCode==39){
		cameras[0].right=true;
	}
	if(e.keyCode==40){
		cameras[0].down=true;
	}
	if(e.keyCode==107){
		cameras[0].in=true;
	}
	if(e.keyCode==109){
		cameras[0].out=true;
	}
}

document.onkeyup = function(e){
	if(document.body.selection!=undefined){
		console.log(document.body.selection);
		textArea.focus();
		document.body.selection.textContent=textArea.value;
	}else{
		document.body.focus();
		
	}
	if(e.keyCode==37){
		cameras[0].left=false;
	}
	if(e.keyCode==38){
		cameras[0].up=false;
	}
	if(e.keyCode==39){
		cameras[0].right=false;
	}
	if(e.keyCode==40){
		cameras[0].down=false;
	}
	if(e.keyCode==107){
		cameras[0].in=false;
	}
	if(e.keyCode==109){
		cameras[0].out=false;
	}
}

var date = new Date();
var frame = 0;
var fps = 0;

Point = function(options){
	this.x = (options != undefined && options.x != undefined)?options.x:0;
	this.y = (options != undefined && options.y != undefined)?options.y:0;
	this.q = (options != undefined && options.q != undefined)?options.q:false;
}

Path = function(options){
	this.pts=(options != undefined && options.pts != undefined)?options.pts:[];
	this.pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
}

Path.prototype.drawPath = function(options){
	var path='M';
	for(var i in this.pts){
		path=path+' '+this.pts[i].x+' ';
		path=(i != this.pts.length-1)?path+this.pts[i].y+' L':path+this.pts[i].y;
	}
}


var baseGrid = 96;



window.onload = function(){

	var camera = new Camera();

	
	document.body.appendChild(textArea);

	document.body.allowed=true;

	document.body.onmouseup = function(e){
		
		if(document.body.clicked ){
			document.body.clicked=false
		}else if(document.body.allowed){
			if(document.body.selection==undefined){
				document.body.clicked=true;
			}
			console.log('CLICK');
			document.body.focus();
			if(document.body.selection!=undefined){
				console.log('DESELECTION');
				document.body.selection.style.backgroundColor='transparent';
				document.body.selection=undefined;
			}
		}
		document.body.mouseIsUp=true;
		if(document.body.allowed && document.body.clicked){
			console.log('CREATE');
			var element = new Element({
				x:e.clientX,
				y:e.clientY,
				z:cameras[0].z,
				w:baseGrid*cameras[0].z,
				h:baseGrid*cameras[0].z
			});
		}
		//console.log(elements.all[elements.all.length-1]);
		elements.all[elements.all.length-1].dom.onmouseover=function(e){
			if(document.body.selected && this!=document.body.selection && document.body.selection!=undefined){
				if(document.body.clicked!=true){
					this.style.backgroundColor='rgba('+255+','+0+','+0+','+0.2+')';
				}
			}else if(this!=document.body.selection){
				if(document.body.clicked!=true){
					this.style.backgroundColor='rgba('+0+','+0+','+255+','+0.2+')';
				}
			}
			document.body.allowed=false;

		}
		elements.all[elements.all.length-1].dom.onmousedown=function(e){
			if(document.body.selected){
				document.body.selected=false;
				if(document.body.selection!=undefined){
					console.log('DESELECTION');
					document.body.selection.style.backgroundColor='transparent';
					if(this.element.x>document.body.selection.element.x){
						document.body.selection.element.w=this.element.x+this.element.w-document.body.selection.element.x;
					}else{
						document.body.selection.element.w=document.body.selection.element.x+document.body.selection.element.w-this.element.x;
						document.body.selection.element.x=this.element.x;
					}
					document.body.selection.element.h=this.element.y+this.element.h-document.body.selection.element.y;
					this.style.display='none';
					document.body.selection=undefined;

				}
			}else if(document.body.clicked!=true){
				console.log('SELECTION');
				document.body.selected=true;
				document.body.selection=this;
				textArea.value=this.textContent;
				this.style.backgroundColor='rgba('+0+','+255+','+0+','+0.2+')';
				
			}
			document.body.allowed=false;
		}
		elements.all[elements.all.length-1].dom.onmouseout=function(e){
			if(this!=document.body.selection){
				this.style.backgroundColor='transparent';

			}
			document.body.allowed=true;
		}
	}

	document.body.onmousedown = function(e){
		document.body.mouseIsUp=false;
		if(elements.all.length>0 && elements.all[elements.all.length-1].dom!=document.body.selection){
			updateCSS(elements.all[elements.all.length-1].dom,{
				backgroundColor:'transparent',
				zIndex:1
			});
		}
	}

	document.body.onmousemove = function(e){
		//console.log(document.body.clicked,document.body.allowed);
		if(elements.all.length>0 && document.body.mouseIsUp && document.body.clicked){
			elements.all[elements.all.length-1].dom.style.zIndex=-1;
			elements.all[elements.all.length-1].x=preview({x:e.clientX}).x;
			elements.all[elements.all.length-1].y=preview({y:e.clientY}).y;
			elements.all[elements.all.length-1].w=preview({w:baseGrid*cameras[0].z}).w;
			elements.all[elements.all.length-1].h=preview({h:baseGrid*cameras[0].z}).h;
			updateCSS(elements.all[elements.all.length-1].dom,{
				backgroundColor:'lavender',
			});
		}
	}

}


var repeat = setInterval(function(){

	for ( var i in cameras ){
		cameras[i].move();
	}
	var base=baseGrid*cameras[0].z;
	if(parseInt(base)>96*2){
		baseGrid=baseGrid/2;
	}
	if(parseInt(base)<96){
		baseGrid=baseGrid*2;
	}
	if(elements.all.length>0){
		for ( var i in elements.all ){
			elements.all[i].update();
		}
	}
	frame++;
	if(date != Date()){
		date = Date();
		fps=frame;
		frame=0;
		//console.log(fps+' fps');
	}

},17);