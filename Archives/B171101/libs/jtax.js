 /* LIBRAIRIE JAVASCRIPT PERSONNELLE * 
 * 
 */

 function check(options,item,Else){
 	for (var i in options){
 		if(i==item){
 			return options[i];
 		}
 	}
 	return Else;
 }

 function declare(list,index,what){
 	(list[index] == undefined)?list[index]=[what]:list[index].push(what);
 }

 function updateCSS(dom,options) {
 	for(var i in options){
 		dom.style[i]=options[i];
 	}
 }
  function updateAttributes(dom,options) {
 	for(var i in options){
 		dom.setAttribute(i,options[i]);
 	}
 }

 function rect(elem){
 	var bodyRect = document.body.getBoundingClientRect(),
 	elemRect = elem.getBoundingClientRect();
 	offset  = elemRect.top - bodyRect.top;
 	var offsetY=offset;
 	offset  = elemRect.left - bodyRect.left;
 	var offsetX=offset;
 	return{
 		x: offsetX,
 		y: offsetY,
 		w: elemRect.width,
 		h: elemRect.height
 	}
 }

 function random(min, max) {
 	return parseInt(Math.random() * (max - min) + min);
 }

 function remove(what,where){

 	for(var i in where){
 		if(where[i]===what){
 			where.splice(i,1);
 			return;
 		}
 	}
 }

 function find(what,where){
 	for(var i in where){
 		if(where[i]===what){
 			return true;			
 		}
 	}
 	return false;
 }

