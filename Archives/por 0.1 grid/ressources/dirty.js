
function drawGrid(base){
	
	var grid = document.getElementById('grid');
	if(grid!=null){
		grid.parentNode.removeChild(grid);
	}
	var svgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	svgElement.id='grid';


var calcY=cameras[0].centreY-(cameras[0].centreY-base*parseInt(window.innerHeight/base)-cameras[0].y)*cameras[0].z;
	var parse=window.innerHeight-calcY;
	for (var i = 0; i < window.innerHeight/base ; i++) {
		var pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
		var path='M';
		var pts=[];
		//cameras[0].centreY-(cameras[0].centreY-this.y-cameras[0].y)*cameras[0].z,
		
		var x=0;
		var y=parse/2+cameras[0].centreY-(cameras[0].centreY-base*i-cameras[0].y)*cameras[0].z;
		var pt = new Point({x:x,y:y});
		pts.push(pt);
		var x=window.innerWidth;
		var pt = new Point({x:x,y:y});
		pts.push(pt);
		for(var j in pts){
			path=path+' '+pts[j].x+' ';
			path=(j != pts.length-1)?path+pts[j].y+' L':path+pts[j].y;
		}
		updateAttributes(pathElement,{
			d:path,
			fill:'none',
			stroke:'rgb(255,192,192)'
		});
		updateCSS(pathElement,{
			strokeWidth:'1'
		});
		
		svgElement.appendChild(pathElement);
		
	}
	var calcX=cameras[0].centreX-(cameras[0].centreX-base*parseInt(window.innerWidth/base)-cameras[0].x)*cameras[0].z;
	var parse=window.innerWidth-calcX;
	for (var i = 0; i < window.innerWidth/base ; i++) {
		var pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
		var path='M';
		var pts=[];
		//cameras[0].centreY-(cameras[0].centreY-this.y-cameras[0].y)*cameras[0].z,
		
		var y=0;
		var x=parse/2+cameras[0].centreX-(cameras[0].centreX-base*i-cameras[0].x)*cameras[0].z;
		var pt = new Point({x:x,y:y});
		pts.push(pt);
		var y=window.innerHeight;
		var pt = new Point({x:x,y:y});
		pts.push(pt);
		for(var j in pts){
			path=path+' '+pts[j].x+' ';
			path=(j != pts.length-1)?path+pts[j].y+' L':path+pts[j].y;
		}
		updateAttributes(pathElement,{
			d:path,
			fill:'none',
			stroke:'rgb(255,192,192)'
		});
		updateCSS(pathElement,{
			strokeWidth:'1'
		});
		
		svgElement.appendChild(pathElement);
	}
	/*
base=base/2;
	for (var i = 0; i < window.innerHeight/base ; i++) {
		var pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
		var path='M';
		var pts=[];
		var pt = new Point({x:0,y:base*i});
		pts.push(pt);
		var pt = new Point({x:window.innerWidth,y:base*i});
		pts.push(pt);
		for(var j in pts){
			path=path+' '+pts[j].x+' ';
			path=(j != pts.length-1)?path+pts[j].y+' L':path+pts[j].y;
		}
		updateAttributes(pathElement,{
			d:path,
			fill:'none',
			stroke:'rgb(232,232,255)'
		});
		updateCSS(pathElement,{
			strokeWidth:'1'
		});
		
		svgElement.appendChild(pathElement);
	}
	
	for (var i = 0; i < window.innerWidth/base ; i++) {
		var pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
		var path='M';
		var pts=[];
		var pt = new Point({x:base*i,y:0});
		pts.push(pt);
		var pt = new Point({x:base*i,y:window.innerHeight});
		pts.push(pt);
		for(var j in pts){
			path=path+' '+pts[j].x+' ';
			path=(j != pts.length-1)?path+pts[j].y+' L':path+pts[j].y;
		}
		updateAttributes(pathElement,{
			d:path,
			fill:'none',
			stroke:'rgb(232,232,255)'
		});
		updateCSS(pathElement,{
			strokeWidth:'1'
		});
		
		svgElement.appendChild(pathElement);
	}*/
	updateCSS(svgElement,{
		width:'100%',
		height:'100%',
	});
	document.body.appendChild(svgElement);
}