var points = [];
Point = function(options){

	/*js*/
	this.x = check('x',options,0);
	this.y = check('y',options,0);
	points.push(this);

	/*dom*/
	this.dom=document.createElementNS('http://www.w3.org/2000/svg', 'circle');
	this.parent=check('parent',options,'points');
	this.type = check('type',options,'L');
	this.dom.id=check('id',options,'point'+points.length);
	this.dom.js=this;

	if(this.type=='L'){
		this.fill='blue'
	}
	if(this.type=='C'){
		this.fill='red'
	}
	if(this.type=='S'){
		this.fill='green'
	}

	document.getElementById(this.parent).appendChild(this.dom);

}

Point.prototype.defineDisplay = function(){
	updateAttributes(this.dom,{
		cx:this.x,
		cy:this.y,
		r:'calc(0.5vw + 0.5vh)',
		fill:this.fill,
	});
}
var svgs = [];

Svg = function(options){
	/*js*/
	/*dom*/
	this.dom=document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	this.dom.id=check('id',options,'svg'+svgs.length);
	this.dom.js=this;
	updateCSS(this.dom,{
		width:'100vw',
		height:'100vh'
	})
	document.body.appendChild(this.dom);
}

var paths = [];

Path = function(options){
	/*js*/
	this.points= check('points',options,[]);
	paths.push(this);
	/*dom*/
	this.dom=document.createElementNS('http://www.w3.org/2000/svg', 'path');
	this.parent=check('parent',options,'svg0');
	this.dom.id=check('id',options,'path'+paths.length);
	this.dom.js=this;
	this.defineShape();
	document.getElementById(this.parent).appendChild(this.dom);
}

Path.prototype.defineShape = function(){

	var shape='M';

	var typeInstance=null;
	var typeIntanceChange=false;
	var lastTypeInstance=null;
	for(var i in this.points){
		if(points[i].type!=typeInstance){
			lastTypeInstance=typeInstance;
			typeInstance=points[i].type;
			typeIntanceChange=true;
		}else{
			typeIntanceChange=false;
		}
		if(typeIntanceChange && typeInstance=='C' && lastTypeInstance!='S'){shape=shape+' '+typeInstance;}
		shape=shape+' '+points[i].x+' '+points[i].y;
		if(typeIntanceChange && typeInstance=='L' || typeIntanceChange && typeInstance=='S'){shape=shape+' '+typeInstance;}
	}

	if(Number.isInteger(parseInt(shape.charAt(shape.length-1))) == false){
		shape=shape.replaceAt(shape.length-1, 'Z');
	}
	
	updateAttributes(this.dom,{
		d:shape,
		//stroke:'red',
		//fill:'none',
		'stroke-width':10,
		'stroke-linejoin':'round',
		'stroke-linecap':'round',
	});


}

window.onload = function(){
	/*common*/
	var fps = 0;
	var date = new Date();
	var frame = 0;
	/*end*/

	svg = new Svg();
	svg = new Svg({id:'points'});
	path = new Path({
		points:[
		new Point({x:20,y:20}),
		new Point({x:100,y:20}),
		new Point({x:200,y:40,type:'C'}),
		new Point({x:200,y:80,type:'C'}),
		new Point({x:150,y:100,type:'S'}),
		new Point({x:180,y:200,type:'C'}),
		new Point({x:100,y:200,type:'S'}),
		new Point({x:40,y:170,type:'C'}),
		new Point({x:20,y:100,type:'L'}),
		new Point({x:20,y:20,type:'L'}),
		]
	})

	console.log(svg);

	var repeat = setInterval(function(){
		/*common*/
		frame++;
		if(date != Date()){
			date = Date();
			fps=frame;
			frame=0;
			//console.log(fps+' fps');
		}
		/*end*/
		for(var i in path.points){
			
			if(path.points[i].type=='S'){
				path.points[i].x=path.points[i].x+random(-1.5,1.5);
				path.points[i].y=path.points[i].y+random(-1.5,1.5);
			}
			path.points[i].defineDisplay();
		}
		path.defineShape();
	},17);
}