var cameras = [];
var em=96;
var Camera = function(options){
	this.position = new Point({x:check('x',options,0),y:check('y',options,0),z:check('z',options,1)});
	this.target = new Point({x:check('targetX',options,window.innerWidth/2),y:check('targetY',options,window.innerHeight/2)});
	this.speed = check('speed',options,8);
	this.traveling={
		left : false,
		up : false,
		right : false,
		down : false,
		front : false,
		back : false
	};
	this.grid = {
		value : check('grid',options,em),
		transfer : check('gridTransfer',options,96),
		ratio : check('ratio',options,1)
	};
	this.layer = 0;
	cameras.push(this);
}
Camera.prototype.interface = function(e,bool){
	var keys=[37,38,39,40,107,109];
	var traveling=['left','up','right','down','front','back'];
	for(var i in keys){
		if(e.keyCode==keys[i]){
			this.traveling[traveling[i]]=bool;

		}
	}
}
Camera.prototype.move = function(){
	var cam = this;
	var conduct = {
		left:function(){cam.position.x=cam.position.x-(1*cam.speed)/cam.position.z},
		up:function(){cam.position.y=cam.position.y-(1*cam.speed)/cam.position.z},
		right:function(){cam.position.x=cam.position.x+(1*cam.speed)/cam.position.z},
		down:function(){cam.position.y=cam.position.y+(1*cam.speed)/cam.position.z},
		front:function(){cam.position.z=cam.position.z*(1+cam.speed/350)},
		back:function(){cam.position.z=cam.position.z/(1+cam.speed/350)},
	};
	for (var i in this.traveling){
		if(this.traveling[i])
		{
			conduct[i]();
		}
	}
	var base=this.grid.transfer*this.position.z;
	if(parseInt(base)>this.grid.value*2){
		this.grid.transfer=this.grid.transfer/2;
		this.layer++;
	}
	if(parseInt(base)<this.grid.value){
		this.grid.transfer=this.grid.transfer*2;
		this.layer--;
	}
	if(elements[parseInt(this.layer/10)]==undefined){
		elements[parseInt(this.layer/10)]=[];
	}
	if(elements[parseInt(this.layer/10)+1]==undefined){
		elements[parseInt(this.layer/10)+1]=[];
	}
	if(elements[parseInt(this.layer/10)-1]==undefined){
		elements[parseInt(this.layer/10)-1]=[];
	}

}
Camera.prototype.tune = function(axis,value){
	return parseInt((this.target[axis]-(this.target[axis]-value+this.position[axis]*this.position.z)/this.position.z)/this.grid.transfer)*this.grid.transfer;
}

var camera = new Camera();