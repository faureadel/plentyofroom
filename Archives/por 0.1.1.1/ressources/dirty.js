



document.onmouseup = function(e){
	var element = new Element({
		x:e.clientX,
		y:e.clientY,
		z:cameras[0].z,
		w:baseGrid*cameras[0].z,
		h:baseGrid*cameras[0].z
	});
}

function preview (options){
	return{
		x:parseInt((cameras[0].centreX-(cameras[0].centreX-check(options,'x',0)+cameras[0].x*cameras[0].z)/cameras[0].z)/baseGrid)*baseGrid,
		y:parseInt((cameras[0].centreY-(cameras[0].centreY-check(options,'y',0)+cameras[0].y*cameras[0].z)/cameras[0].z)/baseGrid)*baseGrid,
		w:check(options,'w',0)/cameras[0].z,
		h:check(options,'h',0)/cameras[0].z,
	}
	
}

document.onmousemove = function(e){
	if(elements.all.length>0){
		elements.all[0].x=preview({x:e.clientX}).x;
		elements.all[0].y=preview({y:e.clientY}).y;
		elements.all[0].w=preview({w:baseGrid*cameras[0].z}).w;
		elements.all[0].h=preview({h:baseGrid*cameras[0].z}).h;
		updateCSS(elements.all[0].dom,{
			backgroundColor:'lavender',
		});
	}
}

document.onkeydown = function(e){
	//console.log(e.keyCode);
	if(e.keyCode==37){
		cameras[0].left=true;
	}
	if(e.keyCode==38){
		cameras[0].up=true;
	}
	if(e.keyCode==39){
		cameras[0].right=true;
	}
	if(e.keyCode==40){
		cameras[0].down=true;
	}
	if(e.keyCode==107){
		cameras[0].in=true;
	}
	if(e.keyCode==109){
		cameras[0].out=true;
	}
}

document.onkeyup = function(e){
	if(e.keyCode==37){
		cameras[0].left=false;
	}
	if(e.keyCode==38){
		cameras[0].up=false;
	}
	if(e.keyCode==39){
		cameras[0].right=false;
	}
	if(e.keyCode==40){
		cameras[0].down=false;
	}
	if(e.keyCode==107){
		cameras[0].in=false;
	}
	if(e.keyCode==109){
		cameras[0].out=false;
	}
}

var date = new Date();
var frame = 0;
var fps = 0;

Point = function(options){
	this.x = (options != undefined && options.x != undefined)?options.x:0;
	this.y = (options != undefined && options.y != undefined)?options.y:0;
	this.q = (options != undefined && options.q != undefined)?options.q:false;
}

Path = function(options){
	this.pts=(options != undefined && options.pts != undefined)?options.pts:[];
	this.pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
}

Path.prototype.drawPath = function(options){
	var path='M';
	for(var i in this.pts){
		path=path+' '+this.pts[i].x+' ';
		path=(i != this.pts.length-1)?path+this.pts[i].y+' L':path+this.pts[i].y;
	}
}


var baseGrid = 96;
var layer = 1;
window.onload = function(){

	var camera = new Camera();
	var element = new Element();
	var element = new Element({w:baseGrid,h:baseGrid});


}
var repeat = setInterval(function(){
//console.log(caca);
	elements.all[0].w=preview({w:baseGrid*cameras[0].z}).w;
	elements.all[0].h=preview({h:baseGrid*cameras[0].z}).h;
	updateCSS(elements.all[1].dom,{
		visibility:'hidden',
	});
	if(elements.all[1].rect.w/elements.all[0].rect.w>1){
		layer++;
		elements.all[1].w=elements.all[1].w/2;
	}
	if(elements.all[1].rect.w/elements.all[0].rect.w<1){
		layer--;
		elements.all[1].w=elements.all[1].w*2;
	}
	//console.log(layer);
	for ( var i in cameras ){
		cameras[i].move();
	}
	var base=baseGrid*cameras[0].z;
	if(parseInt(base)>96*2){
		baseGrid=baseGrid/2;
	}
	if(parseInt(base)<96){
		baseGrid=baseGrid*2;
	}
	if(elements.all.length>0){
		for ( var i in elements.all ){
			elements.all[i].update();
		}
	}
	frame++;
	if(date != Date()){
		date = Date();
		fps=frame;
		frame=0;
		console.log(fps+' fps');
	}

},17);