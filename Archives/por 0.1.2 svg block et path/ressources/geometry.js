var points = {all:[]};

Point = function(options){

	this.x = check('x',options,0);
	this.y = check('y',options,0);
	this.z = check('z',options,1);
	this.target = check('target',options,null);
	if(this.target!=null){
		this.target.push(this);
		/*this.dom=document.createElement('div');
		updateCSS(this.dom,{
			backgroundColor:'blue',
			marginLeft:this.x+'px',
			marginTop:this.y+'px',
			width:32+'px',
			height:32+'px',
			zIndex:999999
		});
		document.body.appendChild(this.dom);*/
	}

	points.all.push(this);
	return this;
}

function newPoint(options){
	var target =  check('target',options,points);
	for(var i in target){
		if(options.x == target[i].x && options.y == target[i].y){
			console.log('point allready exist');
			return;
		}
	}
	var point = new Point(options);
}

var paths=[];
Path = function(options){
	this.dom=document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	this.points= check('points',options,[]);
	this.x=smaller(check('x',this.points));
	this.y=smaller(check('y',this.points));
		this.path='M';
	this.drawPath();
	updateCSS(this.dom,{
		marginLeft:this.x+'px',
		marginTop:this.y+'px',
		width:bigger(check('x',this.points))-smaller(check('x',this.points))+em*60+'px',
		height:bigger(check('y',this.points))-smaller(check('y',this.points))+em*60+'px',

	})
	document.body.appendChild(this.dom);

	paths.push(this);
	console.log(this);
}

Path.prototype.drawPath = function(options){
	this.path='M';
	for(var i = 0 ; i<this.points.length-1; i++){
		this.path=this.path+' '+(this.points[i].x-this.x+em*40/2)+' ';
		this.path=(i != this.points.length-1)?this.path+(this.points[i].y-this.y+em*40/2)+' L':this.path+this.points[i].y;
	}
	var dom = document.createElementNS('http://www.w3.org/2000/svg', 'path');
	updateAttributes(dom,{
		d:this.path,
		stroke:'rgb('+random(0,255)+','+random(0,255)+','+random(0,255)+')',
		fill:'none'
	})
	updateCSS(dom,{
		strokeWidth:em*random(0,40),
		strokeLinecap:'round',
		strokeLinejoin:'round',
	})
	this.dom.appendChild(dom);
}