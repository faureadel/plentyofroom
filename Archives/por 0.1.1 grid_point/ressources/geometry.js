var points = [];

Point = function(options){

	this.x = check('x',options,0);
	this.y = check('y',options,0);
	this.z = check('z',options,1);
	points.push(this);
	return this;
}
var paths=[];
Path = function(options){
	this.points= check('points',options,[]);
}