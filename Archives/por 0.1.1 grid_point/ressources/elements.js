var elements={
	all:[]
};
function newElement(options){
	if(check('camera',options,false)!=false && check('x',options,false)!=false && check('y',options,false)!=false && check('z',options,false)!=false){
		for(var i in points){
			if(options.camera.tune('x',options.x) == points[i].x && options.camera.tune('y',options.y) == points[i].y){
				console.log('point allready exist');
				return;
			}
		}
	}
	var element = new Element(options);


}

Element = function(options){

	this.type = declare(this,elements,check('type',options,'grid'));
	this.name = check('name',options,this.type+elements.all.length);
	elements[this.name]=this;
	this.camera = check('camera',options,cameras[0]);
	this.position = new Point({
		x:this.camera.tune('x',check('x',options,0)),
		y:this.camera.tune('y',check('y',options,0)),
		z:check('z',options,1)
	});
	this.w = check('w',options,0)/this.camera.position.z;
	this.h = check('h',options,0)/this.camera.position.z;
	this.transfer = check('transfer',options,{});
	this.parent = check('parent',options,document.body.getElementsByTagName('main')[0]);
	this.dom = document.createElement(check('dom',options,'div'));
	this.dom.style.zIndex=elements.all.length;
	this.dom.id=this.name;
	this.dom.className=this.type;
	elements.all.push(this);
	return this;
}

Element.prototype.update = function(options){
	this.transfer={
		view:20*(this.position.z-this.camera.position.z)/this.camera.position.z,
		x:this.camera.target['x']-(this.camera.target['x']-this.position.x-this.camera.position.x)*this.camera.position.z,
		y:this.camera.target['y']-(this.camera.target['y']-this.position.y-this.camera.position.y)*this.camera.position.z,
		w:this.w*this.camera.position.z,
		h:this.h*this.camera.position.z,
		opacity:(this.w*this.camera.position.z-this.w*this.position.z)/2500
	}
	if(this.transfer.x + this.transfer.w > this.transfer.view && this.transfer.y + this.transfer.h > this.transfer.view && this.transfer.x < window.innerWidth-this.transfer.view && this.transfer.y < window.innerHeight-this.transfer.view && this.transfer.opacity<1){
		if(document.getElementById(this.name)==null){
			this.parent.appendChild(this.dom);
		}
		updateCSS(this.dom,{
			marginLeft:this.transfer.x+'px',
			marginTop:this.transfer.y+'px',
			width:this.transfer.w+'px',
			height:this.transfer.h+'px',
			opacity:1-this.transfer.opacity,
			fontSize:1/(this.position.z/this.camera.position.z)+'em',
		});
	}else{
		if(document.getElementById(this.name)!=null){
			this.parent.removeChild(this.dom);
		}
	}
}