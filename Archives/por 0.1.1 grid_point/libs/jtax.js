 /* LIBRAIRIE JAVASCRIPT PERSONNELLE * 
 * 
 */

 function check(what,where,otherwise){
 	if (where instanceof Object) {
 		
 		for (var i in where){
 			if(i==what){
 				return where[i];
 			}
 		}
 	}
 	if (where instanceof Array) {
 		var found = [];
 		for (var i in where){
 			found.push(check(what,where[i],otherwise));
 		}
 		return found;
 	}
 	if(otherwise!=undefined)
 		return otherwise;
 }

 function declare(what,where,index){
 	(where[index] == undefined)?where[index]=[what]:where[index].push(what);
 	return index;
 }

 function updateCSS(dom,options) {
 	for(var i in options){
 		dom.style[i]=options[i];
 	}
 }
 function updateAttributes(dom,options) {
 	for(var i in options){
 		dom.setAttribute(i,options[i]);
 	}
 }

 function rect(elem){
 	var bodyRect = document.body.getBoundingClientRect(),
 	elemRect = elem.getBoundingClientRect();
 	offset  = elemRect.top - bodyRect.top;
 	var offsetY=offset;
 	offset  = elemRect.left - bodyRect.left;
 	var offsetX=offset;
 	return{
 		x: offsetX,
 		y: offsetY,
 		w: elemRect.width,
 		h: elemRect.height
 	}
 }

 function random(min, max) {
 	return parseInt(Math.random() * (max - min) + min);
 }

 function remove(what,where){
 	for(var i in where){
 		if(where[i]===what){
 			where.splice(i,1);
 			return;
 		}
 	}
 }

 function find(what,where){
 	for(var i in where){
 		if(where[i]===what){
 			return true;			
 		}
 	}
 	return false;
 }

 function bigger(where){
 	var what = Infinity*-1;
 	for(var i in where){
 		if(where[i]>=what){
 			what=where[i];			
 		}
 	}
 	return what;
 }
 function smaller(where){
 	var what = Infinity;
 	for(var i in where){
 		if(where[i]<=what){
 			what=where[i];			
 		}
 	}
 	return what;
 }
 function getOnly(what,where){
 	for(var i in where){
 		console.log(where[i]);
 	}
 }

