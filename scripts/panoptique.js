function w(){
	return window.innerWidth;
}
function h(){
	return window.innerHeight;
}
function cx(){
	return Math.floor(w()/2);
}
function cy(){
	return Math.floor(h()/2);
}

Keys = function(){
}

Keys.prototype.newKey = function(key){
	this[key]=true;
}

keys = new Keys();

function drawCircle(where,color,pos,width,id,zPos){
	if(document.getElementById(id)){
		var circle = document.getElementById(id);
		updateCSS(circle,{
			marginLeft:pos[0]-width/2+'px',
			marginTop:pos[1]-width/2+'px',
			width:width+'px',
			height:width+'px',
			fontSize:width/2+'px',
			backgroundColor:color,
			zIndex:zPos,
			'border-radius': '50%'
		});
		var stringId='circle'+(coord.length-1);
		if(id == stringId){
			//console.log('hello');
			updateCSS(circle,{
			backgroundColor:'red'
		});
		}
	}else{
		var circle = document.createElement('div');
		circle.id=id;
		document.body.appendChild(circle);
	}
}

function drawLine(points,id,size,zIndex){
console.log(size);
	if(document.getElementById(id)){
		var line = document.getElementById(id);
		updateAttributes(line,{
			x1:points[0][0],
			y1:points[0][1],
			x2:points[1][0],
			y2:points[1][1],
			stroke:"orange",
			'stroke-width':size+'px',
			'stroke-linecap':'round',
		});
		updateCSS(line.parentNode,{
			zIndex:zIndex-1
		});
		
		updateCSS(line,{
			zIndex:zIndex-1
		});
	}else{
		var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
		updateCSS(svg,{
			width:'100%',
			height:'100%',
		});
		var line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
		line.id = id
		svg.appendChild(line);
		document.body.appendChild(svg);
	}

}

function rotate2d(pos,rad){
	var x = pos[0];
	var y = pos[1];

	var s = Math.sin(rad);
	var c = Math.cos(rad);

	return [x*c - y*s,y*c + x*s]
}

Cam = function(options){
	define(this,options,{
		pos:[0,0,0],
		scale:[0,0,1],
		rot:[0,0]
	});
}

Cam.prototype.events = function(event) {
	if(event.type=='mousemove'){
		var x = event.movementX;
		var y = event.movementY;
		x/=cx(); 
		y/=cy();
		this.rot[0]+=y*this.scale[2]; this.rot[1]+=x*this.scale[2];
	}
};

Cam.prototype.update = function(s,key) {

	if (key['a']){ this.pos[1]-=s*this.scale[2] };
	if (key['e']){ this.pos[1]+=s*this.scale[2] };

	var x = s*Math.sin(this.rot[1])*this.scale[2];
	var y = s*Math.cos(this.rot[1])*this.scale[2];

	if (key['z']){ this.pos[0]+=x; this.pos[2]+=y };
	if (key['s']){ this.pos[0]-=x; this.pos[2]-=y };

	if (key['q']){ this.pos[0]-=y; this.pos[2]+=x };
	if (key['d']){ this.pos[0]+=y; this.pos[2]-=x };

	if (key['+']){ this.scale[2]/=1+s/3;};
	if (key['-']){ this.scale[2]*=1+s/3};

	if (key['ArrowUp']){ this.scale[1]+=s*50*this.scale[2];};
	if (key['ArrowDown']){ this.scale[1]-=s*50*this.scale[2];};

	if (key['ArrowLeft']){ this.scale[0]+=s*50*this.scale[2];};
	if (key['ArrowRight']){ this.scale[0]-=s*50*this.scale[2];};

	if (key['Backspace']){ 
		if(this.scale[2]>1){
			this.scale[2]/=1+s/3;
		}
		if(this.scale[2]<1){
			this.scale[2]*=1+s/3;
		}
		this.scale[0]-=this.scale[0]*0.1;
		this.scale[1]-=this.scale[1]*0.1

	};
	
}

Cam.prototype.spatialize = function(x,y,z){

	x-=this.pos[0];
	y-=this.pos[1];
	z-=this.pos[2];

	hRot = rotate2d([x,z],this.rot[1])
	vRot = rotate2d([y,hRot[1]],this.rot[0])

	var f = cx()/vRot[1];
	x = hRot[0]*f;
	y = vRot[0]*f;
	return {
		x:x,
		y:y,
		z:vRot[1],
	}
}

length = 2;
coord = []
length = 50;

for(var i = 0 ; i < length ; i++){
	coord.push([random(-length/10,length/10),random(-length/10,length/10),random(-length/10,length/10),random(10,500),random(-2,2),random(-2,2),random(-2,2),random(1,100)]);
}
//verts = [[-1,-1,-1],[1,-1,-1],[1,1,-1],[-1,1,-1],[-1,-1,1],[1,-1,1],[1,1,1],[-1,1,1]]
//colors = ['blue','red','green','yellow','black','brown','lightblue','lightgreen','pink','orange','purple','grey']
//edges = [[0,1],[1,2],[2,3],[3,0],[0,4],[1,5],[2,6],[3,7],[4,5],[5,6],[6,7],[7,4]]
console.log(coord);

coord.push([0,0,0]);
//coord = []

var cam = new Cam({pos:[0,0,-5]});

window.onload = function(){

	document.body.style.backgroundColor='white'

}

window.repeat = setInterval(function(){
	var points=[];
	var hidden=[];
	coord[coord.length-1][0]=cam.pos[0]+cam.scale[0]*0.003/cam.scale[2];//cam.rot[1];
	coord[coord.length-1][1]=cam.pos[1]+cam.scale[1]*0.003/cam.scale[2];//cam.rot[0];
	coord[coord.length-1][2]=cam.pos[2]+3/cam.scale[2];
	for(var i in coord){

		//coord[i][0]+=coord[i][2]/1000;

		var pos=cam.spatialize(coord[i][0],coord[i][1],coord[i][2]);
		var zIndex = -parseInt(pos.z*coord.length);
		//size = 40;
		dist=Math.sqrt(Math.pow(cam.pos[0]-coord[i][0],2)+Math.pow(cam.pos[1]-coord[i][1],2)+Math.pow(cam.pos[2]-coord[i][2],2));
		size=w()/100*(10/dist);

		pos.x=cam.scale[0]-(cam.scale[0]-pos.x+cam.scale[0]*cam.scale[2])/cam.scale[2];
		pos.y=cam.scale[1]-(cam.scale[1]-pos.y+cam.scale[1]*cam.scale[2])/cam.scale[2];

		size/=cam.scale[2];
		points.push([cx()+pos.x,cy()+pos.y,pos.z,size,zIndex]);
		if(pos.z>1 && pos.x>-cx()-size && pos.x<w()-cx()+size && pos.y>-cy()-size && pos.y<h()-cy()+size && size>0.1){
			drawCircle(screen,'blue',[cx()+pos.x,cy()+pos.y],size,'circle'+i,zIndex);
		}else{
			if(document.getElementById('circle'+i)!=null){
				document.getElementById('circle'+i).parentNode.removeChild(document.getElementById('circle'+i));
			}
		}
	}

	for(var i = 0 ; i < points.length-2 ; i++){
		if(points[i][2]>1&& points[i+1][2]>1 ){
			drawLine([points[i],points[i+1]],'line'+i,points[i][3],points[i][4]);
		}else{
			if(document.getElementById('line'+i)!=null){
				var parent = document.getElementById('line'+i).parentNode;
				document.getElementById('line'+i).parentNode.removeChild(document.getElementById('line'+i));
				parent.parentNode.removeChild(parent);
			}
		}
	}

	cam.update(0.05,keys);

},17)

document.onkeydown = function(e){
	if(keys[e.key]==undefined){
		keys.newKey(e.key);
	}else{
		keys[e.key]=true;
	}
}

document.onkeyup = function(e){
	console.log(e.key);
	if(keys[e.key]==undefined){
		keys.newKey(e.key);
	}else{
		keys[e.key]=false;
	}
	if(e.keyCode==32){
		coord.push([0,0,0]);
	}
}

var mousedown = false

document.onmousedown = function(e){
	if(mousedown){
		
	}else{
		mousedown=true
	}
}
document.onmouseup = function(e){
	if(mousedown){
		mousedown=false
	}
}



window.onmousemove = function(e){	
	if(mousedown){		
		cam.events(e);
	}
}

